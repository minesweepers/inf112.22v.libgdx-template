# Description
Pigsweepers is a platform game where 1-2 players move through the world, picking up diamonds and hearts while killing the enemy pigs. The goal of the game is to get to the last lane and kill the final bosspig.

## Maven Setup
This project comes with a working Maven `pom.xml` file. You should be able to import it into Eclipse using *File → Import → Maven → Existing Maven Projects* (or *Check out Maven Projects from SCM* to do Git cloning as well). You can also build the project from the command line with `mvn` and test it with `mvn clean test`.

## Importing
- Clone the project from https://git.app.uib.no/minesweepers/inf112.22v.libgdx-template
- Copy ssh / https key
- Import through either command line or an ide like eclipse / intellij

### Running
- Right click on the main.java file -> Run As -> Java Application

### Testing
- Right click on the src/test/java folder -> Run As -> JUnit Test
