# Deloppgave 1

## Komeptanse
Alle i gruppen går andre år datateknologi på UIB eller tilsvarende. Gruppen har litt tidligere erfaring med spillutvikling i programmer som Pygame, Unity og Godot.

## Roller

### Team leder
- William

### Test ansvarlige
- Kasper
- Jørgen

### Dev's
- Simon
- Vegard
- Elend

Trenger en team leder for organisering av gruppen. Alle skriver tester til sin egen kode, men de test ansvarlige skal sjekke at alle tester nok og godt nok. Dev's hopper rundt og hjelper der det trengs. Å lage flere roller føles symbolske/unødvendige, men endringer kan skje senere i utviklingen viss det er nødvendig.

# Deloppgave 2

## Prosjektmetodikk
Vi velger noen ulike elementer fra både Kanban og Scrum:
- Kanban board på Trello (Kanban)
- To Do delen er organisert sånn at det øverste har størst prioritet (Scrum)

Prøver oss frem med disse relene foreløbig og ser om vi trenger flere/færre senere.

## Tidlig organisering
- Møtes hver Torsdag kl. 12-14 for å jobbe med prosjektet, i tillegg til arbeid over discord når det trengs.
- Vi har opprettet vår egen discord kanal for kommunikasjon mellom møter.
- Alle oppgaver som ikke trenger at noe annen kode blir skrivet på forhånd blir fordelt likt over gruppemedlemmene og de som blir ferdig tidlig går de over til å hjelpe de som ikke er ferdige. Trello boardet blir oppdatert underveis for å organisere dette.
- På gruppetimene og møtene på torsdag går me gjennom de nye oppdateringene og ser gjennom de.
- Planen er at alle jobber på sin egen branch og merger med master-branchen når en oppgave er gjort.

# Deloppgave 3

## Overordnet mål for applikasjonen
Lage et plattformspill der man skal samle alle myntene på brettet samtidig som man unngår fiender/hindringer.

## Brukerhistorier i priotitert rekkefølge
- "Som utvikler trenger jeg å vise et spillbrettet slik at jeg kan plassere spillobjekter i verdenen"
- Arbeidsoppgave: Vise et spillbrett. 
- Akseptansekriterier: Når man kjører main, skal man kunne se et spillbrett
- -----------------------
- "Som spiller trenger jeg å skille spillerfiguren fra brettet så jeg vet hvor jeg er."
- Arbeidsoppgave: Vise spiller på spillbrett. 
- Akseptansekriterier: Når man kjører main, skal man kunne se spillerfiguren på spillbrettet
- -----------------------
- "Som programmør trenger jeg å holde styr på spillerfiguren sin posisjon så jeg kan vise den på riktig plass på skjermen."
- Arbeidsoppgave: Vise spiller på spillbrett. 
- Akseptansekriterier: Når man skaper en player med ulike koordinater stemmer spillfigurens posisjon på skjermen med koordinatene
- -----------------------
- "Som spiller trenger jeg at spillfiguren reagerer på input så jeg kan flytte meg der jeg vil."
- Arbeidsoppgave: Få spilleren til å kunne bevege seg til høyre og venstre. 
- Akseptansekriterier: Når man trykker på piltastene beveger spillerfiguren seg i den retningen man har trykket på.
- -----------------------
- "Som spiller trenger jeg at spillfiguren kan hoppe så jeg kan unngå fare."
- Arbeidsoppgave: Få spiller til å hoppe. 
- Akseptansekriterier: Når man trykker på pil opp hopper spillfiguren.
- -----------------------
- "Som programmør trenger jeg å vite om spillfiguren er i kontakt med bakken slik at spilleren ikke kan hoppe i luften."
- Arbeidsoppgave: Lag en hasGroundContact() sjekk.
- Akseptansekriterier: Når man trykker på pil opp hopper spilleren, men bare hvis han er på bakken.

# Deloppgave 4

## Manuelle tester
 Starter spillet?
1. Kjør “Main.java”
2. Se at et nytt program starter

Gravity?
1. Kjør “Main.java”
2. Se om spiller faller nedover mot bakken

Venstre?
1. Kjør “Main.java”
2. Press “venstre” å se om spiller beveger seg mot venstre

Høyre?
1. Kjør “Main.java”
2. Press “høyre” å se om spiller beveger seg mot høyre

Hopp?
1. Kjør “Main.java”
2. Press “spacebar” å se om spiller beveger seg oppover(hoppet)

## Hvordan bygge og runne koden
- Klon prosjektet fra https://git.app.uib.no/minesweepers/inf112.22v.libgdx-template
- Kopier ssh/https key
- Import gjennom enten kommandolinje eller en ide som eclipse/intellij
- Kjør main.java filen


# Deloppgave 5

## Oppsummering
Det å bruke Tiled til map design gikk relativt lett.
Fant texures på nettet som gjorde at kartet så veldig ut tidlig i utviklingen.

Fysikk og kollisjon med objekter var det en del problemer (box2d).
Også litt styr med branches og merge collisions i starten.
Noen problemer med at spillet fungerte på windows pc-er men ikke mac (kollisjon).

Til neste oblig vil me få til lokal multiplayer, introdusere hindringer som kan drepe spilleren osv.
(Resten av MVP kravene i oppgaven)


## Trello board
https://trello.com/b/5sr7xptZ/ms-board
