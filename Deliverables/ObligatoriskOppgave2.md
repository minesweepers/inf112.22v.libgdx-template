# Oblig 2 – *Pigsweepers*

* Team: *Minesweepers* (Gruppe 1): *Jørgen M. Storum, Elend Kasem, Kaspar Mober, Vegard Dæmring, Simon Andersen, William Amundsen Winterstø*


## Referat fra team-møter
Dato: *17/03/2022*, Tilstede: *Alle*
- Diskusjon: *Navn på prosjektet, korleis ein siste boss ville blitt, om man må drepe griser og samle diamanter for å fortsette til neste brett*
- Avgjørelser: *Bestemte at navnet på prosjektet/spillet skal være "Pigsweepers"*
- Neste møte: *Få til start og game over screen*

Dato: *21/03/2022*, Tilstede: *Alle*
- Diskusjon: *Hva vi skal ha som stretch goal, mål for spillet, hvordan vi skal få til online multiplayer, end-game mål*
- Avgjørelser: *Stretch goal blir online multiplayer*
- Neste møte: *Fullføre text oppgavene, skrive tester*

Dato: *24/03/2022*, Tilstede: *Alle untatt Simom (var ikke i form)*
- Diskusjon: *Oppdaget en bug der spillet kræsjer når man dreper alle fiender på kartet og går gjennom døren til neste brett, diskuterte en del om korleis vi kan fikse dette*
- Avgjørelser: *Endret priorieten til å fjerne buggen*
- Neste møte: *Fortsette å fjerne MVP-bugsene og fortsette å utvikle online multiplayer*


## Deloppgave 1: Team og prosjekt

##### Eksisterende roller
Rollene har fungert fint så langt, så vi har ikke sett noe behov for å endre på dem.

##### Andre roller
Ser enda ikke noe behov for å legge til nye roller. Rollene vi har innebærer:
- Team lead: Generell organisering av gruppen. Ansvar for Trello boardet osv. Ikke noe man gjør hele tiden så blir også en slags Dev.
- Test ansvarlige: Generell QA og skal sjekke at alle tester nok og godt nok.
- Dev's: Hopper rundt og hjelper der det trengs. I startfasen har de fleste på gruppen vert dev's i realiteten.

##### Erfaringer og prosjektmetodikk
Det er noen erfaringer vi gjorde tidlig. Vi hardkodet en del i begynnelsen for å teste som endte med at vi ikke hadde ett godt rammeverk å bygge utifra, og mye av koden måtte skrives om igjen. Ellers har valgene våre fungert ganske bra. Fremover vil vi fokusere mer på god kode som ikke krever for mye jobb å utvide.

##### Gruppedynamikk
Fungerer fint, sjeldent uenigheter

##### Kommunikasjon i gruppen
Fungerer fint

##### Retrospektiv om prosjektstruktur
Til nå har vi fått til en fungerende MVP og jobber i sammen jevnlig. Det er god kommunikasjon i discorden om møter og idéer som kan implementeres. Forbedringer kan være bedre kommunikasjon rundt pushing sånn at vi unngår å få merge conflict så ofte.

##### Committer
Det kan være forskjell i hvor mange commits hver gruppemedlemmer har. Dette er fordi noen jobber mer en andre med f.eks design av kart som man kanskje bare pusher når man er ferdig, mens en skrivefeil i textfilene kan være en commit. Det kan fort også bli litt parprogrammering som ikke er synlig bare av å se på committmeldingene. Arbeidsmengden i gruppen er relativt likt fordelt.


## Deloppgave 2

### Stretch goal
Vi vil prøve å implementere online multiplayer til spillet. Mest sannsynlig ved hjelp av kryonet.

### MVP og annet

##### MVP fremgang
Vi har prioritert mvp kravene i den rekkefølgen de er skrevet opp i. Siden forrige gang har vi nesten gjort ferdig MVP kravene (mangler bare game over screen akkuratt nå, men kan hende denne er implementert innen fristen). Hvordan vi skal prioritere ny funksjonalitet etter MVP har ikke blitt diskutert enda.

##### Brukerhistorier, tidligere brukerhistorier ligger i ObligatoriskOppgave1.md
- "Som utvikler trenger jeg at spilleren interagerer med terreng slik at spilleren ikke faller gjennom gulvet eller går gjennom alle vegger".
- Arbeidsoppgave: Gi terreng og spiller hitboxer som interagerer med hverandre. 
- Akseptansekriterier: Når man kjører main, skal spilleren ikke dette gjennom kartet med en gang. Når spilleren kolliderer med en vegg skal han stoppe opp.
- -----------------------
- "Som spiller trenger jeg at spilleren kan plukke opp diamantene på kartet og få mer score slik at jeg kan gjøre framgang i spillet".
- Arbeidsoppgave: Legge til diamanter på kartet som fjernes og øker scoren når spilleren plukker dem opp.
- Akseptansekriterier: Når man kjører main, skal det være diamanter på kartet. Når spilleren kommer i kontakt med en diamant, forsvinner den, og scoren på sjermen økes.
- -----------------------
- "Som utvikler trenger jeg å plassere fientlige griser på kartet som interagerer med spilleren og terrenget slik at spilleren møter noe form for motstand i spillet".
- Arbeidsoppgave: Legge til fientlige griser på kartet som interagerer med terreng (gulv og vegger stopper dem) og kan skade spilleren ved kontakt.
- Akseptansekriterier: Når man kjører main, skal det være griser på kartet. Grisene skal gå frem og tilbake og snu rettning når de kolliderer med terreng. Når spilleren kommer i kontakt med en gris, tar spilleren skade, og man kan se livet til spilleren på sjermen gå ned.
- -----------------------
- "Som spiller trenger jeg et angrep som skader grisene slik at jeg kan kjempe imot".
- Arbeidsoppgave: Gi spilleren et angrep som gjør skade på grisene. 
- Akseptansekriterier: Etter man har kjørt main kan spilleren trykker på pil-ned knappen for å angripe. Hvis en gris er nært nok angrepet til å kollidere med angreps-hitboxen vil grisen ta skade. Under angrepet og når skaden blir tatt vil man se animasjoner av det for bekreftelse.
- -----------------------
- "Som utvikler trenger jeg at spilleren kan dø slik at det er konsekvenser av å ta skade".
- Arbeidsoppgave: Gjør slik at når spillenen sitt liv går til null så dør spilleren. 
- Akseptansekriterier: Etter man har kjørt main og spilleren sitt liv går til null vil spilleren dø. Når det skjer ser man døds-animasjonen og gameoverskjermen dukker opp..
- -----------------------
- "Som spiller trenger jeg en siste boss slik at jeg har et mål å jobbe mot".
- Arbeidsoppgave: Legge til en "bossgris" som dukker opp på slutten av spillet. 
- Akseptansekriterier: Etter man har kjørt main og drept alle de vanlige grisene, eller har kommet seg gjennom alle banene, vil spilleren havne på det siste brettet sammen med bossgrisen. Spillet slutter når spilleren dreper bossgrisen.
- -----------------------
- "Som spiller trenger jeg en måte å komme meg fra ett brett til et annet slik at jeg kan gjøre fremgang i spillet".
- Arbeidsoppgave: Legge til dører på brettet som spilleren bruker til å komme til neste brett. 
- Akseptansekriterier: Etter man har kjørt main og nådd enden av et brett, kan spilleren interagere med døren for å laste inn neste brett.
- -----------------------
- "Som spiller tenger jeg en start-skjerm slik at jeg kan f.eks velge om jeg vil spille multiplayer eller ikke".
- Arbeidsoppgave: Legge til en start-skjerm.
- Akseptansekriterier: Etter man har kjørt main er start-skjermen det første som dukker opp. Det faktiske spillet starter når man har gjort de nødvendige valgene i start-skjermen.
- -----------------------
- "Som spiller tenger jeg en gameover-skjerm slik at jeg kan velge om jeg vil starte på nytt eller avslutte spillet".
- Arbeidsoppgave: Legge til en gameover-skjerm.
- Akseptansekriterier: Etter man har kjørt main og spilleren har dødd dukker gameover-skjermen opp. Velger spilleren å fortsette blir de tatt tilbake til start-skjermen.
- -----------------------
- "Som utvikler trenger jeg en multiplayer funksjon slik at flere spillere kan sammarbeide i spillet".
- Arbeidsoppgave: Legge til multiplayer lokalt og over nettet.
- Akseptansekriterier: Etter man har kjørt main og kan man velge multiplayer lokalt eller over nettet. Når man starter spillet vil det være to spillbare figurer på brettet. Velger man lokal multiplayer vil begge spillere bruke samme tastatur til å bevege seg. Velger man multiplayer over nettet kan hver av spillerene styre med hver sin maskin.

##### Prioriteringer fremover
Fokuserer på å fjerne MVP bugs og å nå vårt strech-goal som er online-multiplayer.

##### Rekkefølge i MVP-krav
Ble implementert mer eller mindre i oppgitt rekkefølge. Vi hadde ingen grunn til å gjøre det annerledes siden oppsettet virket naturlig.

##### Bugs i MVP-kravene
Spillet kjører muligens i bakgrunnen i gameover-skjermen. Både start og gameover-skjermene jobbes fortsatt med. Knappene i gameover-skjermen fungerer ikke. 


## Deloppgave 3

##### Dette har vi fikset siden sist
- Bedre refleksjon rundt prosjektmetodikk.
- Project board er satt til public.
- Bedre beskrivelse av arbeidsoppgaver i nye brukerhistorier.
- Lagd en bedre README.md fil
- Ryddet opp ubrukt kode

##### Linux, Windows og OS X
Alle på gruppen jobber med Windows/OS X maskiner så vi vet de fungerer. Har ikke testet på Linux enda.

#### Manuelle tester
Starter spillet?
1. Kjør “Main.java”
2. Se at et nytt program starter

Gravity?
1. Kjør “Main.java” og klikk på skjermen for å fortsette forbi start-skjermen.
2. Se om spiller faller nedover mot bakken

Venstre?
1. Kjør “Main.java” og klikk på skjermen for å fortsette forbi start-skjermen.
2. Press “venstre” å se om spiller beveger seg mot venstre

Høyre?
1. Kjør “Main.java” og klikk på skjermen for å fortsette forbi start-skjermen.
2. Press “høyre” å se om spiller beveger seg mot høyre

Hopp?
1. Kjør “Main.java” og klikk på skjermen for å fortsette forbi start-skjermen.
2. Press “spacebar” å se om spiller beveger seg oppover(hoppet)

Terreng?
1. Kjør “Main.java” og klikk på skjermen for å fortsette forbi start-skjermen.
2. Spilleren ikke dette gjennom kartet med en gang.
3. Gå bort til en vegg og bekreft at spilleren stopper og ikke går gjennom den.

Diamanter og score?
1. Kjør “Main.java” og klikk på skjermen for å fortsette forbi start-skjermen.
2. Observer at det er diamanter på kartet.
3. Beveg spilleren bort til en diamant.
4. Bekreft at diamanten forsvinner når spilleren kommer i kontakt med den og at scoren på sjermen økes.

Griser?
1. Kjør “Main.java” og klikk på skjermen for å fortsette forbi start-skjermen.
2. Observer at det er griser på kartet.
3. Observer at grisene går frem og tilbake og snur rettning når de kolliderer med terreng.
4. Observer at grisene endrer de går i når spilleren kommer nært nok.
5. Observer at spilleren blir angrepet, tar skade, og at livet til spilleren på skjermen går ned når han kommer i kontakt med en gris.

Spillerangrep?
1. Kjør “Main.java” og klikk på skjermen for å fortsette forbi start-skjermen.
2. Trykk på pil-ned knappen og observer at spilleren angriper.
3. Angrip ved siden og vendt mot en gris. Viss du er nærme nok skal du observere at grisen ta skade.

Spillerdød?
1. Kjør “Main.java” og klikk på skjermen for å fortsette forbi start-skjermen.
2. La en gris angripe deg til livet til spilleren når null.
3. Observer at spilleren dør og a gameover-skjermen dukker opp.

Siste bossgris? (Ikke implementert, foreløbig er det bossgriser på tilfeldige brett)
1. Kjør “Main.java” og klikk på skjermen for å fortsette forbi start-skjermen.
2. Drep alle griser på alle brett / kom deg gjennom alle banene.
3. Observer at spilleren havner på et siste brettet sammen med bossgrisen.
4. Observer at spillet slutter når spilleren dreper bossgrisen.

Dører?
1. Kjør “Main.java” og klikk på skjermen for å fortsette forbi start-skjermen.
2. Gå til enden av brettet og interager med døren (ned-pil)
3. Observer at neste brett blir lastet inn sammen med spiller og nye fiender.

Start-skjerm?
1. Kjør “Main.java”
2. Observer at start-skjermen er det første som dukker opp.
3. Klikk på skjermen og observer at det faktiske spillet starter.

Gameover-skjerm?
1. Kjør “Main.java” og klikk på skjermen for å fortsette forbi start-skjermen.
2. Ta skade til du dør og observer at gameover-skjermen dukker opp.

##### Statiske analyseverktøy
Ikke brukt til nå.

### Trello Board
https://trello.com/b/5sr7xptZ/pigsweepers-board
