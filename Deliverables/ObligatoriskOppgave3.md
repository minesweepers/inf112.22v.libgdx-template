# Oblig 3 – *Pigsweepers*

* Team: *Minesweepers* (Gruppe 1): *Jørgen M. Storum, Elend Kasem, Kaspar Mober, Vegard Dæmring, Simon Andersen, William Amundsen Winterstø*


## Referat fra team-møter
Dato: *28/03/2022*, Tilstede: *Alle untatt Simom (var ikke i form)*
- Diskusjon: *Generell diskusjon rundt tester og bugs*
- Avgjørelser: *Ingen avgjørelser bare fortsatt å jobbe med eksisterende problemer*
- Neste møte: *Prøve å få til pauseskjerm og ein måte å restarte spillet på*

Dato: *31/03/2022*, Tilstede: *Alle untatt Kaspar, Jørgen (var ikke i form) og Elend*
- Diskusjon: *Hva som mangler til en demo/innleveringen*
- Avgjørelser: *Fokuset fremover er tester og å få til en demo*
- Neste møte: *Fullføre alle screens (gameover-screen spesielt)*

Dato: *04/04/2022*, Tilstede: *Alle*
- Diskusjon: *Problem med at JUnit testene ikke fungerer på Mac*
- Avgjørelser: *Skal legge til quit/main meny option i gameover-skjermen*
- Neste møte: *Implementere gameover-screen knapper*

Dato: *07/04/2022*, Tilstede: *Alle*
- Diskusjon: *Hvordan vi skal gjøre tester og snakk om hvordan vi skal bruke noen nye features*
- Avgjørelser: *Nye maps laget for testing så det er lettere å teste spillet, og en Win-screen*
- Neste møte: *Implementasjon av de nye featurene vi diskuterte*

## Deloppgave 1: Team og prosjekt

##### Roller
Rollene har fungert fint så langt, så vi har ikke sett noe behov for å endre på dem.

Rollene vi har innebærer:
- Team lead: Generell organisering av gruppen. Ansvar for Trello boardet osv. Ikke noe man gjør hele tiden så blir også en slags Dev.
- Test ansvarlige: Generell QA og skal sjekke at alle tester nok og godt nok.
- Dev's: Hopper rundt og hjelper der det trengs. I startfasen har de fleste på gruppen vert dev's i realiteten.

##### Erfaringer og prosjektmetodikk
Det er noen erfaringer vi gjorde tidlig. Vi hardkodet en del i begynnelsen for å teste som endte med at vi ikke hadde ett godt rammeverk å bygge utifra, og mye av koden måtte skrives om igjen. Ellers har valgene våre fungert ganske bra. Fremover vil vi fokusere mer på god kode som ikke krever for mye jobb å utvide.

##### Gruppedynamikk og kommunikasjon 
Fungerer fint, sjeldent uenigheter

##### Retrospektiv om prosjektstruktur
Til nå har vi fått til en fungerende MVP og jobber i sammen jevnlig. Det er god kommunikasjon i discorden om møter og idéer som kan implementeres. Trello boardet er nyttig for oversikt og forhindrer at vi ikke jobber med det samme og kan fordele arbeidskraften effektivt. Forbedringer kan være bedre kommunikasjon rundt pushing sånn at vi unngår å få merge conflict så ofte. Vi kunne utnyttet trello boardet fullt tidligere siden det ble litt ignorert de første ukene. Fremover vil vi prøve å utnytte alle verkøyene gitt til oss bedre.

##### Committer
Det kan være forskjell i hvor mange commits hver gruppemedlemmer har. Dette er fordi noen jobber mer en andre med f.eks design av kart som man kanskje bare pusher når man er ferdig, mens en skrivefeil i textfilene kan være en commit. Det kan fort også bli litt parprogrammering som ikke er synlig bare av å se på committmeldingene. Arbeidsmengden i gruppen er relativt likt fordelt.


## Deloppgave 2

### Stretch goal
Vi vil prøve å implementere online multiplayer til spillet, mest sannsynlig ved hjelp av kryonet. Lokal multiplayer allerede implementert.

### MVP og annet

##### MVP fremgang
Vi har prioritert mvp kravene i den rekkefølgen de er skrevet opp i. Vi så ingen grunn til å gjøre det annerledes siden oppsettet virket naturlig. Siden forrige gang har vi gjort ferdig MVP kravene. Når det kommer til ny funksjonalitet prioriteres stretch goalet, men dette er ikke hovedfokuset siden prosjektets svakhet nå er tester og ikke funksjonalitet.

##### Brukerhistorier
- "Som utvikler trenger jeg å vise et spillbrettet slik at jeg kan plassere spillobjekter i verdenen"
- Arbeidsoppgave: Vise et spillbrett. 
- Akseptansekriterier: Når man kjører main, skal man kunne se et spillbrett
- -----------------------
- "Som spiller trenger jeg å skille spillerfiguren fra brettet så jeg vet hvor jeg er."
- Arbeidsoppgave: Vise spiller på spillbrett. 
- Akseptansekriterier: Når man kjører main, skal man kunne se spillerfiguren på spillbrettet
- -----------------------
- "Som programmør trenger jeg å holde styr på spillerfiguren sin posisjon så jeg kan vise den på riktig plass på skjermen."
- Arbeidsoppgave: Vise spiller på spillbrett. 
- Akseptansekriterier: Når man skaper en player med ulike koordinater stemmer spillfigurens posisjon på skjermen med koordinatene
- -----------------------
- "Som spiller trenger jeg at spillfiguren reagerer på input så jeg kan flytte meg der jeg vil."
- Arbeidsoppgave: Få spilleren til å kunne bevege seg til høyre og venstre. 
- Akseptansekriterier: Når man trykker på piltastene beveger spillerfiguren seg i den retningen man har trykket på.
- -----------------------
- "Som spiller trenger jeg at spillfiguren kan hoppe så jeg kan unngå fare."
- Arbeidsoppgave: Få spiller til å hoppe. 
- Akseptansekriterier: Når man trykker på pil opp hopper spillfiguren.
- -----------------------
- "Som programmør trenger jeg å vite om spillfiguren er i kontakt med bakken slik at spilleren ikke kan hoppe i luften."
- Arbeidsoppgave: Lag en hasGroundContact() sjekk.
- Akseptansekriterier: Når man trykker på pil opp hopper spilleren, men bare hvis han er på bakken.
- -----------------------
- "Som utvikler trenger jeg at spilleren interagerer med terreng slik at spilleren ikke faller gjennom gulvet eller går gjennom alle vegger".
- Arbeidsoppgave: Gi terreng og spiller hitboxer som interagerer med hverandre. 
- Akseptansekriterier: Når man kjører main, skal spilleren ikke dette gjennom kartet med en gang. Når spilleren kolliderer med en vegg skal han stoppe opp.
- -----------------------
- "Som spiller trenger jeg at spilleren kan plukke opp diamantene på kartet og få mer score slik at jeg kan gjøre framgang i spillet".
- Arbeidsoppgave: Legge til diamanter på kartet som fjernes og øker scoren når spilleren plukker dem opp.
- Akseptansekriterier: Når man kjører main, skal det være diamanter på kartet. Når spilleren kommer i kontakt med en diamant, forsvinner den, og scoren på sjermen økes.
- -----------------------
- "Som utvikler trenger jeg å plassere fientlige griser på kartet som interagerer med spilleren og terrenget slik at spilleren møter noe form for motstand i spillet".
- Arbeidsoppgave: Legge til fientlige griser på kartet som interagerer med terreng (gulv og vegger stopper dem) og kan skade spilleren ved kontakt.
- Akseptansekriterier: Når man kjører main, skal det være griser på kartet. Grisene skal gå frem og tilbake og snu rettning når de kolliderer med terreng. Når spilleren kommer i kontakt med en gris, tar spilleren skade, og man kan se livet til spilleren på sjermen gå ned.
- -----------------------
- "Som spiller trenger jeg et angrep som skader grisene slik at jeg kan kjempe imot".
- Arbeidsoppgave: Gi spilleren et angrep som gjør skade på grisene. 
- Akseptansekriterier: Etter man har kjørt main kan spilleren trykker på pil-ned knappen for å angripe. Hvis en gris er nært nok angrepet til å kollidere med angreps-hitboxen vil grisen ta skade. Under angrepet og når skaden blir tatt vil man se animasjoner av det for bekreftelse.
- -----------------------
- "Som utvikler trenger jeg at spilleren kan dø slik at det er konsekvenser av å ta skade".
- Arbeidsoppgave: Gjør slik at når spillenen sitt liv går til null så dør spilleren. 
- Akseptansekriterier: Etter man har kjørt main og spilleren sitt liv går til null vil spilleren dø. Når det skjer ser man døds-animasjonen og gameoverskjermen dukker opp..
- -----------------------
- "Som spiller trenger jeg en siste boss slik at jeg har et mål å jobbe mot".
- Arbeidsoppgave: Legge til en "bossgris" som dukker opp på slutten av spillet. 
- Akseptansekriterier: Etter man har kjørt main og drept alle de vanlige grisene, eller har kommet seg gjennom alle banene, vil spilleren havne på det siste brettet sammen med bossgrisen. Spillet slutter når spilleren dreper bossgrisen.
- -----------------------
- "Som spiller trenger jeg en måte å komme meg fra ett brett til et annet slik at jeg kan gjøre fremgang i spillet".
- Arbeidsoppgave: Legge til dører på brettet som spilleren bruker til å komme til neste brett. 
- Akseptansekriterier: Etter man har kjørt main og nådd enden av et brett, kan spilleren interagere med døren for å laste inn neste brett.
- -----------------------
- "Som spiller tenger jeg en start-skjerm slik at jeg kan f.eks velge om jeg vil spille multiplayer eller ikke".
- Arbeidsoppgave: Legge til en start-skjerm.
- Akseptansekriterier: Etter man har kjørt main er start-skjermen det første som dukker opp. Det faktiske spillet starter når man har gjort de nødvendige valgene i start-skjermen.
- -----------------------
- "Som spiller tenger jeg en gameover-skjerm slik at jeg kan velge om jeg vil starte på nytt eller avslutte spillet".
- Arbeidsoppgave: Legge til en gameover-skjerm.
- Akseptansekriterier: Etter man har kjørt main og spilleren har dødd dukker gameover-skjermen opp. Velger spilleren å fortsette blir de tatt tilbake til starten av brettet.
- -----------------------
- "Som utvikler trenger jeg en multiplayer funksjon slik at flere spillere kan sammarbeide i spillet".
- Arbeidsoppgave: Legge til multiplayer lokalt og over nettet.
- Akseptansekriterier: Etter man har kjørt main og kan man velge multiplayer lokalt eller over nettet. Når man starter spillet vil det være to spillbare figurer på brettet. Velger man lokal multiplayer vil begge spillere bruke samme tastatur til å bevege seg. Velger man multiplayer over nettet kan hver av spillerene styre med hver sin maskin.
- -----------------------
- "Som spiller trenger jeg en "instillinger" meny slik at jeg kan endre på fps, vsync, display mode etc.".
- Arbeidsoppgave: Legge til en instillinger meny.
- Akseptansekriterier: Etter man har kjørt main og kan man velge instillinger i start-skjermen. Viss man endrer display mode til en custom størrelse vil man se at spillviduet endrer seg til den størrelsen, viss man endrer fps vil man se at den endrer seg når man starter spillet etc.


##### Bugs i MVP-kravene
Gameover-skjermen jobbes fortsatt med. Knappene i gameover-skjermen fungerer ikke, man bare trykker på skjermen for å fortsette (mulig det er fikset til innleveringen).


## Deloppgave 3

##### Dette har vi fikset siden sist (Har ikke fått vurdering av Oblig2 enda så dette er de samme forbedringene som ble levert der)
- Bedre refleksjon rundt prosjektmetodikk.
- Project board er satt til public.
- Bedre beskrivelse av arbeidsoppgaver i nye brukerhistorier.
- Lagd en bedre README.md fil
- Ryddet opp ubrukt kode

##### Linux, Windows og OS X
Alle på gruppen jobber med Windows/OS X maskiner så vi vet de fungerer. Har ikke testet på Linux enda.

#### Manuelle tester
Starter spillet?
1. Kjør “Main.java”.
2. Se at et nytt program starter.

Setup testMode
1. Kjør “Main.java” og klikk på "OPTIONS" og toggle "DEBUG" til true, så velg "APPLY AND EXIT".
2. klikk på "LOCAL PLAY" og se at valgene som vises er "test: items, doors" "test2: enemy, bombs and boxes" og "Main Menu"
3. velg "test1: items, doors" og se at spillet starter.

Alle testene etter "Setup testMode" er gjordt i test mode/debug mode. Du skal bare bytte debug til true en gang, så skal den huske instillingene hver gang du åpner spillet.
Når testing er ferdig skal debug bli skrudd av igjen og spillet skal kunne bli spilt som vanlig igjen.

Det er forventer at testene blir utført i rekkefølge. Vist de blir gjordt i rekkefølge kan steg 1 bli ignorert.

Alle tests spilles i 2 player og jeg vil referere til spilleren med rødt skjegg som "Main spiller" spilleren og den med brunt skjegg som "2player".

Hopp og Gravity?
1. Kjør “Main.java” og klikk på "LOCAL PLAY" og "test1: items, doors".
2. Trykk på “pil-opp” å se om main spilleren hopper og deretter faller ned igjen til bakken.
2. Trykk på “W" å se om 2player hopper og deretter faller ned igjen til bakken.

Venstre?
1. Kjør “Main.java” og klikk på "LOCAL PLAY" og "test1: items, doors".
2. Press “venstre-pil” å se om main spiller beveger seg mot venstre.
3. se at main spilleren "ser" mot venstre.
4. Press “A” å se om spiller beveger seg mot venstre.
5. se at 2player "ser" mot venstre.

Høyre?
1. Kjør “Main.java” og klikk på "LOCAL PLAY" og "test1: items, doors".
2. Press “høyre-pil” å se om main spiller beveger seg mot høyre.
3. se at spilleren "ser" mot høyre.
4. Press “D” å se om 2player beveger seg mot høyre.
5. se at 2player "ser" mot høyre.

Attack?
1. Kjør “Main.java” og klikk på "LOCAL PLAY" og "test1: items, doors".
2. Press "pil ned" å se om main spilleren utfører et angrep.
3. Press "S" å se om 2player utfører et angrep.
4. Utfør et angrep på en medspiller og se at du fly opp i været.

Diamanter, score og "pickup sound"?
1. Ha på lyd på maskinen.
2. Kjør “Main.java” og klikk på "LOCAL PLAY" og "test1: items, doors".
3. Observer at det er diamanter på brettet.
4. Beveg spilleren bort til en diamant og bekreft at diamanten forsvinner når spilleren kommer i kontakt med den og at scoren på sjermen økes.
5. Hør om den blir spilt av en "pickup sound".

Hjerter, HP og "pickup sound"?
1. Ha på lyd på maskinen.
2. Kjør “Main.java” og klikk på "LOCAL PLAY" og "test1: items, doors".
3. Observer at det er hjerter på brettet.
4. bekreft at en av spillerene mangler ett hjerte.
5. Beveg main spilleren bort til et hjerte og bekreft at hjertet forsvinner når spilleren kommer i kontakt med den og at helsen til spilleren på skjermen økes.
6. Hør om den blir spilt av en "pickup sound".

Terreng?
1. Kjør “Main.java” og klikk på "LOCAL PLAY" og "test1: items, doors".
2. Sjekk at spilleren ikke detter gjennom bakken med en gang.
3. Beveg spilleren bort til en vegg(“venstre-pil”/"A" eller “høyre-pil”/"D") og bekreft at spilleren stopper ved kontakt og ikke går gjennom den.

Dører?
1. Kjør “Main.java” og klikk på "LOCAL PLAY" og "test1: items, doors".
2. Ikke plukk opp kronen som ligger ved siden av døren.
3. Gå til enden av brettet og interager med døren ved å trykke på "mellomrom" å se at den ikke virker.
4. Nå plukk opp kronen og interager med døren på nytt.
5. Observer at neste brett(test2) blir lastet inn sammen med spiller og fiender.

Griser spillerdød og gameover-screen?
1. Kjør “Main.java” og klikk på "LOCAL PLAY" og "test2: enemy, bombs and boxes".
2. Observer at det er griser på kartet.
3. Observer at grisene går frem og tilbake og snur rettning når de kolliderer med terreng.
4. Observer at grisene endrer retning de går i når spilleren kommer nært nok.(gå en spiller til veggen på venstre side å se at grisene snur seg mot spilleren om og om igjen.)
5. Hopp inn i en av "grise pittene"
6. Observer at spilleren blir angrepet, tar skade, og at livet til spilleren på skjermen går ned når han blir angripet av en gris.
7. Når spilleren sine liv er 0, observer at spilleren dør og at "Gameover-skjermen" dukker opp.

Spillerangrep?
1. Kjør “Main.java” og klikk på "LOCAL PLAY" og "test2: enemy, bombs and boxes".
2. Angrip ved siden og vendt mot en gris. Viss du er nærme nok skal du observere at grisen ta skade.
3. Etter noen vellykkete angrep skal grisen dø og forsvinne.

Bomber og Bokser?
1. Kjør “Main.java” og klikk på "LOCAL PLAY" og "test2: enemy, bombs and boxes".
2. Observer at det ligger to bomber på den øverste platformen samt tre bokser oppå hverandre.
3. Hopp opp til platformen og utfør et angrep en bomben. 
4. Den skal "fly" et stykke bort å sprenge etter en liten stund.
5. Vist en bombe sin eksplisjon treffer boksene skal de forsvinne.
7. Viss spilleren eller bossgrisen er nærme nok bomben når den eksploderer vil du også obervere at de tar skade.

Bossgris og win-screen?
1. Kjør “Main.java” og klikk på "LOCAL PLAY" og "test2: enemy, bombs and boxes".
2. Hopp til platformen med bomber og bokser på.
3. Send bomber ned på "bossen" til den dør.
4. Observer at bossen dropper en krone, og plukk den opp.
5. Gå til døren og trykk "mellomrom" og se at "win skjermen" kommer fram.


### Trello Board
https://trello.com/b/5sr7xptZ/pigsweepers-board

### Grafikk og lyd

##### Mesteparten er hentet fra open-source asset packs:
- https://pixelfrog-assets.itch.io/kings-and-pigs
- https://pixelfrog-assets.itch.io/treasure-hunters

##### Men vi har også laget ting selv:
- Start-skjerm, Gameover-skjerm, Pause-skjerm, Gamesettings-skjerm (tekstfont er open-sorce)
- Player 2 modell og animasjon (modifikasjon av player 1 modell og animasjon)
- TankPig og WingPig (modell og animasjon, modifikasjon av EnemyPig modell og animasjon)
- Modifikasjoner av textures i bossrommet

