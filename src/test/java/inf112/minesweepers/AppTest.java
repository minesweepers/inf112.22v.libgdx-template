package inf112.minesweepers;

import static org.junit.jupiter.api.Assertions.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.math.Vector2;
import org.junit.jupiter.api.*;

public class AppTest {
	public static final Vector2 spawnPos = new Vector2(0,0);
	public static final TestGame testGame = new TestGame();

	@BeforeAll
	public static void beforeAll(){
		Lwjgl3ApplicationConfiguration configuration = new Lwjgl3ApplicationConfiguration();
		configuration.setInitialVisible(false);
		new Lwjgl3Application(testGame, configuration);

		HeadlessApplicationConfiguration conf = new HeadlessApplicationConfiguration();
		new HeadlessApplication(testGame, conf);
	}

	/**
	 * Test that Gdx.files are initiated
	 */
	@Test
	public void testFiles() {
		assertNotNull(Gdx.files);
	}

	/**
	 * Test that graphics gl is working
	 * */
	@Test
	public void testGL20(){
		assertNotNull(Gdx.gl);
	}
}