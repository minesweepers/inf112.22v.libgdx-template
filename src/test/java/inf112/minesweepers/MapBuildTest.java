package inf112.minesweepers;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import mapObjects.Door;
import mapObjects.Platform;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MapBuildTest {
    private World world;

    @BeforeAll
    static void beforeAll(){
        AppTest.beforeAll();
    }

    @BeforeEach
    void beforeEach(){
        world = new World(new Vector2(0,-10),true);
    }

    @Test
    void testMapBuilding(){
        TiledMap map = new TmxMapLoader().load("maps/testMap.tmx");

        LevelBuilder lb = new LevelBuilder(world);
        lb.buildTiledMap(map);

        int doorCount = 0;
        int platformCount = 0;
        for (Body body : LevelBuilder.tileBodies){
            if (body.getUserData() instanceof Door){
                doorCount++;
            }
            else if (body.getUserData() instanceof Platform){
                platformCount++;
            }
        }
        //testMap.tmx has 10 platforms and 1 door
        assertEquals(1,doorCount);
        assertEquals(10,platformCount);
    }
}
