package inf112.minesweepers;

import static org.junit.jupiter.api.Assertions.*;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import gameObjects.gameActors.GameActor;
import gameObjects.gameActors.enemies.BossPig;
import gameObjects.gameActors.enemies.EnemyPig;
import gameObjects.gameActors.enemies.WingPig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GameActorTest {
    private World world;

    @BeforeAll
    static void beforeAll(){
        AppTest.beforeAll();
    }

    @BeforeEach
    void beforeEach(){
        world = new World(new Vector2(0,-10),true);
    }

    @Test
    void enemyPigTest(){
        EnemyPig enemyPig = new EnemyPig(AppTest.spawnPos, world);
        assertNotNull(enemyPig);

        testActor(enemyPig);
    }

    @Test
    void wingPigTest(){
        WingPig wingPig = new WingPig(AppTest.spawnPos, world);
        assertNotNull(wingPig);

        testActor(wingPig);
    }

    @Test
    void bossPigTest(){
        BossPig bossPig = new BossPig(AppTest.spawnPos, world);
        assertNotNull(bossPig);

        testActor(bossPig);
    }

    private void testActor(GameActor gameActor){
        assertNotNull(gameActor.getPosition());
        assertNotNull(gameActor.getBody());

        gameActor.setHealth(10);
        gameActor.damage(5);
        assertEquals(5, gameActor.getHealth());

        gameActor.die();
        assertTrue(gameActor.isDeleted);
    }
}
