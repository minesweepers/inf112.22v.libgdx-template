package inf112.minesweepers;

import static org.junit.jupiter.api.Assertions.*;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import gameObjects.items.Bomb;
import gameObjects.items.Crown;
import gameObjects.items.Diamond;
import gameObjects.items.Heart;
import org.junit.jupiter.api.*;

public class ItemTest {
    private World world;

    @BeforeAll
    static void beforeAll(){
        AppTest.beforeAll();
    }

    @BeforeEach
    void beforeEach(){
    	Pigsweepers game = new Pigsweepers();
        world = new World(new Vector2(0,-10),true);
    }
    
    @Test
    void testBomb(){
        Bomb bomb = new Bomb(AppTest.spawnPos, world);
        assertNotNull(bomb);
        assertNotNull(bomb.getBody());

        bomb.ignite();
        assertTrue(bomb.isIgnited());
        bomb.explode();
    }
    
    @Test
    void testCrown(){
        Crown crown = new Crown(AppTest.spawnPos, world);
        assertNotNull(crown);

        assertNotNull(crown.getBody());

        crown.pickUp(null);
        assertTrue(crown.isDeleted);
    }

    @Test
    void testDiamond(){
        Diamond diamond = new Diamond(AppTest.spawnPos, world);
        assertNotNull(diamond);

        assertNotNull(diamond.getBody());

        diamond.pickUp(null);
        assertTrue(diamond.isDeleted);
    }

    @Test
    void testHeart(){
        Heart heart = new Heart(AppTest.spawnPos, world);
        assertNotNull(heart);

        assertNotNull(heart.getBody());

        heart.pickUp(null);
        assertTrue(heart.isDeleted);
    }
}

