package inf112.minesweepers;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

public class Main {

	public static Lwjgl3Application app;
	
    public static void main(String[] args) {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setTitle("Minesweepers");
        cfg.setForegroundFPS(300); //limit when focused
        cfg.setIdleFPS(30);        //limit when minimized

        cfg.useVsync(false);
        cfg.setResizable(true);
        cfg.setWindowedMode(1024,512); //same value as camera tiled map resolution Game::57
        app = new Lwjgl3Application(new Pigsweepers(), cfg);
    }
}