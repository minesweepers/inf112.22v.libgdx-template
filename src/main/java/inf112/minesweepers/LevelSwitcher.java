package inf112.minesweepers;

import gameObjects.*;
import gameObjects.gameActors.players.Player;
import gameScreens.GameScreen;
import helper.LevelMap;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.physics.box2d.World;
import helper.Constants;

import java.util.Objects;

public class LevelSwitcher {
    public String currentLevel;
    public String nextLevel;

    private final Pigsweepers game;
    private final LevelBuilder levelBuilder;
    private OrthogonalTiledMapRenderer renderer;

    private static long stageStartTime;
    private static long stageStartFps;

    public LevelSwitcher(Pigsweepers game, World world, String startLevel) {
        this.game = game;

        levelBuilder = new LevelBuilder(world);
        LevelBuilder.tileBodies.clear();

        LevelMap.createDoorMap();

        changeLevel(startLevel);
    }

    public static long getStageTime(){
        int stageEndFps = Gdx.graphics.getFramesPerSecond();
        long stageEndTime = Gdx.graphics.getFrameId();

        return (stageEndTime - stageStartTime) / ((stageEndFps + stageStartFps) / 2);
    }

    //Change level if nextLevel is different
    public void checkLevel() {
        if (!Objects.equals(currentLevel, nextLevel)) {
            long time = game.save.getLong("time");
            game.save.putLong("time", time + getStageTime());

            for (GameObject g : GameScreen.gameObjectsToRender) {
                if (g instanceof Player p) {
                    if (p.mainPlayer) {
                        game.save.putInteger("playerHp", p.getHealth());
                    } else {
                        game.save.putInteger("player2Hp", p.getHealth());
                    }
                }
            }
            //Save new level
            game.save.putInteger("score", Hud.score);
            game.save.putInteger("diamonds", Hud.diamonds);
            game.save.putString("currentLevel", nextLevel);
            game.save.flush();

            //Load new level
            changeLevel(nextLevel);
        }
    }

    public void changeLevel(String mapName) {
        currentLevel = mapName;
        nextLevel = currentLevel;

        stageStartFps = Gdx.graphics.getFramesPerSecond();
        stageStartTime = Gdx.graphics.getFrameId();

        //Load map and get tile dimensions as well as the tile object layer
        TiledMap map = new TmxMapLoader().load("maps/" + mapName + ".tmx");

        renderer = new OrthogonalTiledMapRenderer(map, Constants.SCALE);
        renderer.setView(game.camera);

        //We want to keep the created player instances
        for (GameObject gameObject : GameScreen.gameObjectsToRender) {
            if (!(gameObject instanceof Player)) {
                //Destroy sensors for enemies
                gameObject.isDeleted = true;
            }
        }

        //Destroy old platforms and spawn gameObjects and new platforms
        levelBuilder.destroyTileBodies();
        levelBuilder.buildTiledMap(map);

        levelBuilder.spawnPlayers(map, GameScreen.gameObjectsToRender);
        levelBuilder.spawnGameObjects(map, GameScreen.gameObjectsToRender);
        GameScreen.hud.changeLevelName(currentLevel);
    }

    public OrthogonalTiledMapRenderer getRenderer() {
        return renderer;
    }
}
