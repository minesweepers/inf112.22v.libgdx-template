package inf112.minesweepers;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import gameObjects.gameActors.GameActor;
import gameObjects.gameActors.enemies.BossPig;
import gameObjects.gameActors.enemies.Enemy;
import gameObjects.gameActors.players.Player;
import gameObjects.gameActors.sensors.AttackHitSensor;
import gameObjects.gameActors.sensors.EnemySensor;
import gameObjects.gameActors.sensors.ExplosionSensor;
import gameObjects.gameActors.sensors.GroundSensor;
import gameObjects.items.*;
import gameScreens.GameScreen;
import helper.AudioHandler;
import mapObjects.Door;
import mapObjects.Platform;

public record ContactHandler(LevelSwitcher levelSwitcher) implements ContactListener {

    @Override
    public void beginContact(Contact contact) {
        checkDoorContact(contact, true);
        checkGroundContact(contact, true);
        checkItemCollision(contact);

        checkBombCollision(contact);
        checkExplosionCollision(contact);

        checkAttackCollision(contact);
        checkDetectionCollision(contact);
    }

    @Override
    public void endContact(Contact contact) {
        checkDoorContact(contact, false);
        checkGroundContact(contact, false);
        checkDetectionCollision(contact);
    }

    @Override
    public void preSolve(Contact contact, Manifold manifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse contactImpulse) {
    }


    private void checkDetectionCollision(Contact contact) {
        //Enemy senses/detects a Player
        if (isPlayerClassCollision(contact, EnemySensor.class)) {
            EnemySensor enemySensor = getCollisionObject(contact, EnemySensor.class);
            Player player = getCollisionObject(contact, Player.class);
            if (player == null || enemySensor == null) {
                return;
            }

            if (enemySensor.getEnemy().isDead) {
                return;
            }

            if (enemySensor.getSensorType().equals(EnemySensor.SensorType.ATTACK_SENSOR)) {
                enemySensor.setAtkForEnemy();
            } else if (enemySensor.getSensorType() == EnemySensor.SensorType.TURN_SENSOR) {

                if (enemySensor.getEnemy() instanceof BossPig bossPig) {
                    float dist = player.getPosition().y - bossPig.getPosition().y;
                    if (dist > 0) {
                        bossPig.jump(1 + dist);
                    }
                }

                if (player.getPosition().x > enemySensor.getEnemy().getPosition().x
                        && enemySensor.getEnemy().getMovementSpeed() < 0) {
                    enemySensor.turnEnemy();
                } else if (player.getPosition().x < enemySensor.getEnemy().getPosition().x
                        && enemySensor.getEnemy().getMovementSpeed() > 0) {
                    enemySensor.turnEnemy();
                }
            }
        }
    }

    private void checkAttackCollision(Contact contact) {
        //Attack hitbox collides with another fixture
        if (isClassCollision(contact, AttackHitSensor.class)) {
            AttackHitSensor attackHitSensor = getCollisionObject(contact, AttackHitSensor.class);
            if (attackHitSensor == null) return;

            GameActor attackerGameActor = attackHitSensor.getGameActor();
            if (attackerGameActor == null) return;

            //Attack hits item
            if (isClassCollision(contact, Item.class)) {
                Item item = getCollisionObject(contact, Item.class);
                if (item == null) return;

                if (!(item instanceof CannonBall) && !(item instanceof Box)) {
                    if (item instanceof Bomb bomb) {
                        bomb.ignite();
                        item.getBody().setLinearVelocity(attackHitSensor.getAtkOffSetX() * 5, 5);
                    } else {
                        item.getBody().setLinearVelocity(attackHitSensor.getAtkOffSetX() * 2, 6 - item.getBody().getMass());
                    }
                }
            } else if (isClassCollision(contact, GameActor.class)) {
                //GameActor that got hit by an attack
                GameActor hitGameActor = getCollisionObject(contact, GameActor.class);
                if (hitGameActor == null) return;

                //Prevent enemies from hitting each other and disable PVP
                if (hitGameActor instanceof Enemy && attackerGameActor instanceof Enemy) {
                    return;
                }
                if (hitGameActor instanceof Player p && attackerGameActor instanceof Player) {
                    p.getBody().setLinearVelocity(attackHitSensor.getAtkOffSetX() * 2, 5);
                    return;
                }

                if (!hitGameActor.isDead) {
                    //The boss pig is immune to melee dmg
                    if (hitGameActor instanceof BossPig) {
                        return;
                    }
                    damageGameActor(hitGameActor, attackHitSensor.getDamage());
                }
            }
        }
    }

    private void checkExplosionCollision(Contact contact) {
        //Bomb explosion sensor
        if (isClassCollision(contact, ExplosionSensor.class)) {
            ExplosionSensor explosionSensor = getCollisionObject(contact, ExplosionSensor.class);
            if (explosionSensor == null) return;

            //Make bomb explosion hurt GameActors
            if (isClassCollision(contact, GameActor.class)) {
                GameActor gameActor = getCollisionObject(contact, GameActor.class);
                if (gameActor == null) return;

                Vector2 bombPos = explosionSensor.getBomb().getPosition();
                Vector2 enemyPos = gameActor.getPosition();
                if (!explosionSensor.getBomb().isExploded()) {
                    int damage = (int) Math.max(0, 20 - 22 * bombPos.dst(enemyPos));

                    if (gameActor instanceof Player) {
                        damageGameActor(gameActor, damage / 2);
                    } else {
                        damageGameActor(gameActor, damage);
                    }
                }
            }
            //Make bomb explosion destroy boxes, ignite other bombs and destroy cannons
            else if (isClassCollision(contact, Box.class)) {
                Box box = getCollisionObject(contact, Box.class);
                if (box != null) {
                    box.destroy();
                }
            } else if (isClassCollision(contact, Bomb.class)) {
                Bomb bomb = getCollisionObject(contact, Bomb.class);
                if (bomb != null) {
                    bomb.ignite();
                }
            } else if (isClassCollision(contact, Cannon.class)) {
                Cannon cannon = getCollisionObject(contact, Cannon.class);
                if (cannon != null) {
                    cannon.destroy();
                }
            }
        }
    }

    private void checkBombCollision(Contact contact) {
        //Special movement for bossBig when in contact with a bomb
        if (isClassCollision(contact, Bomb.class)) {
            Bomb bomb = getCollisionObject(contact, Bomb.class);
            if (bomb == null) return;
            if (isClassCollision(contact, BossPig.class)) {
                BossPig bossPig = getCollisionObject(contact, BossPig.class);
                if (bossPig == null) return;

                //Panic when bomb is ignited else jump above it
                if (bomb.isIgnited()) {
                    bossPig.panic();
                } else {
                    bossPig.jump(1);
                }
            } else if (isClassCollision(contact, Enemy.class)) {
                Enemy enemy = getCollisionObject(contact, Enemy.class);
                if (enemy != null) {
                    enemy.isAttacking = true;
                }
            }
        }
    }

    private void checkGroundContact(Contact contact, boolean isBeginContact) {
        //IActor touches a platform enemy or item
        if (isClassCollision(contact, GroundSensor.class)) {
            if (isClassCollision(contact, Platform.class) || isClassCollision(contact, Enemy.class) || isClassCollision(contact, Item.class)) {
                GroundSensor groundSensor = getCollisionObject(contact, GroundSensor.class);
                if (groundSensor == null) return;

                if (isBeginContact) {
                    groundSensor.getGameActor().increaseContact();
                } else {
                    groundSensor.getGameActor().decreaseContact();
                }
            }
        }
    }

    private void checkItemCollision(Contact contact) {
        //Cannonball hits an GameActor or Box
        if (isClassCollision(contact, CannonBall.class)) {
            CannonBall cb = getCollisionObject(contact, CannonBall.class);
            if (cb == null) return;
            if (isClassCollision(contact, GameActor.class)) {
                GameActor gameActor = getCollisionObject(contact, GameActor.class);
                if (gameActor == null) return;
                damageGameActor(gameActor, 1);
                cb.destroy();
            } else if (isClassCollision(contact, Box.class)) {
                Box box = getCollisionObject(contact, Box.class);
                if (box == null) return;
                box.getBody().setLinearVelocity(-1f, 0);
            }
        }
        //Player comes in contact with an Item
        else if (isPlayerItemCollision(contact)) {
            Item item = getCollisionObject(contact, Item.class);
            if (item == null) return;

            if (item instanceof Bomb || item instanceof Box || item instanceof Cannon) {
                return;
            }
            if (item instanceof Note) {
                GameScreen.hud.note = true;
            }
            item.pickUp(getPlayer(contact));
            AudioHandler.playPickup();
        }
    }

    private void checkDoorContact(Contact contact, boolean isBeginContact) {
        //Player comes in contact with Door
        if (isPlayerDoorCollision(contact)) {
            Player player = getPlayer(contact);
            Door door = getCollisionObject(contact, Door.class);
            if (player == null || door == null) return;
            if (isBeginContact) {
                door.setSwitcher(levelSwitcher);
                player.doorContact(door);
            } else {
                player.doorContact(null);
            }
        }
    }

    private void damageGameActor(GameActor gameActor, int damage) {
        if (gameActor.gotHit) {
            return;
        }
        gameActor.damage(damage);
        gameActor.gotHit = true;
        gameActor.stateTimeGotHit = 0;

        if (gameActor.getHealth() <= 0) {
            gameActor.isDead = true;
            gameActor.stateTimeAtkAndDeath = 0;
        }
        if (gameActor instanceof Player p) {
            GameScreen.hud.updateLives(p);
        } else if (gameActor instanceof Enemy enemy) {
            GameScreen.hud.addScore(enemy.getValue());
        }

        AudioHandler.playHurt();
    }

    /**
     * Returns the object from the given class c
     */
    private <T> T getCollisionObject(Contact contact, Class<? extends T> c) {
        Object obj1 = contact.getFixtureA().getBody().getUserData();
        Object obj2 = contact.getFixtureB().getBody().getUserData();

        if (c.isInstance(obj1)) {
            return c.cast(obj1);
        }
        if (c.isInstance(obj2)) {
            return c.cast(obj2);
        }
        return null;
    }


    /**
     * Checks if a Player is in contact with the given parameter class
     */
    private boolean isPlayerClassCollision(Contact contact, Class<?> c) {
        if (contact.getFixtureA() == null || contact.getFixtureB() == null) {
            return false;
        }
        Object obj1 = contact.getFixtureA().getBody().getUserData();
        Object obj2 = contact.getFixtureB().getBody().getUserData();

        if (isPlayerCollision(contact)) {
            return c.isInstance(obj1) || c.isInstance(obj2);
        }
        return false;
    }

    /**
     * Check if given class is part of contact
     */
    private <T> boolean isClassCollision(Contact contact, Class<T> c) {
        if (contact.getFixtureA() == null || contact.getFixtureB() == null) {
            return false;
        }
        Object obj1 = contact.getFixtureA().getBody().getUserData();
        Object obj2 = contact.getFixtureB().getBody().getUserData();

        if (c.isInstance(obj1)) {
            return true;
        }
        return c.isInstance(obj2);
    }

    private boolean isPlayerCollision(Contact contact) {
        return isClassCollision(contact, Player.class);
    }

    private boolean isPlayerDoorCollision(Contact contact) {
        return isPlayerClassCollision(contact, Door.class);
    }

    private boolean isPlayerItemCollision(Contact contact) {
        return isPlayerClassCollision(contact, Item.class);
    }

    private Player getPlayer(Contact contact) {
        Object obj1 = contact.getFixtureA().getBody().getUserData();
        Object obj2 = contact.getFixtureB().getBody().getUserData();

        if (obj1 instanceof Player) {
            return (Player) obj1;
        }
        if (obj2 instanceof Player) {
            return (Player) obj2;
        }
        return null;
    }
}
