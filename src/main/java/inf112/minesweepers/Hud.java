package inf112.minesweepers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.viewport.FitViewport;
import gameObjects.gameActors.players.Player;
import helper.Constants;

public class Hud {
	public Stage stage;
	public static Integer score;
	public static Integer diamonds;

	private final Pigsweepers game;
	private FitViewport fitViewport;
	private Table table;

	private final Texture healthBarTexture;
	private final Texture heartTexture;
	private final Texture bigNote;
	private Label scoreLabel;
	private Label levelLabel;
	public boolean note=false;

	private int hp = 5;
	private int hp2 = 5;

	public Hud(Pigsweepers game) {
		this.game = game;
		healthBarTexture = new Texture("gui/HealthBar.png");
		heartTexture = new Texture("gui/Heart.png");
		bigNote = new Texture("gui/NoteBig.png");

		if (game.save.contains("playerHp")){
			hp = game.save.getInteger("playerHp");
		}

		if (game.mpOn){
			if (game.save.contains("player2Hp")){
				hp2 = game.save.getInteger("player2Hp");
			}
		}

		if (game.save.contains("score")){
			score = game.save.getInteger("score");
		}
		else{
			score = 0;
		}

		if (game.save.contains("diamonds")){
			diamonds = game.save.getInteger("diamonds");
		}
		else {
			diamonds = 0;
		}

		createTable();
		setHud(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}

	public void setHud(int width, int height) {
		fitViewport = new FitViewport(width, height);
		stage = new Stage(fitViewport, game.batch);
		stage.addActor(table);
	}

	public void renderHp(){
		game.batch.setProjectionMatrix(game.camera.combined);
		game.batch.begin();



		//p2
		if (game.mpOn){
			//Draw player 1 health bar
			game.batch.draw(healthBarTexture, game.viewport.getWorldWidth()/2-(getWidth(healthBarTexture)), game.viewport.getWorldHeight()-(34*Constants.SCALE),
					getWidth(healthBarTexture), getHeight(healthBarTexture));
			for (int i = 0; i<hp; i++){
				game.batch.draw(heartTexture,game.viewport.getWorldWidth()/2 + (i*16*Constants.SCALE + 28 * Constants.SCALE - getWidth(healthBarTexture)), game.viewport.getWorldHeight() - 22*Constants.SCALE,
						getWidth(heartTexture), getHeight(heartTexture));
			}

			game.batch.draw(healthBarTexture, game.viewport.getWorldWidth()/2, game.viewport.getWorldHeight()-(34*Constants.SCALE),
					getWidth(healthBarTexture), getHeight(healthBarTexture));
			for (int i = 0; i<hp2; i++){
				game.batch.draw(heartTexture,game.viewport.getWorldWidth()/2 + (i*16*Constants.SCALE + 28 * Constants.SCALE), game.viewport.getWorldHeight() - 22*Constants.SCALE,
						getWidth(heartTexture), getHeight(heartTexture));
			}
		}
		else{
			//Draw player 1 health bar
			game.batch.draw(healthBarTexture, game.viewport.getWorldWidth()/2-(getWidth(healthBarTexture)/2), game.viewport.getWorldHeight()-(34*Constants.SCALE),
					getWidth(healthBarTexture), getHeight(healthBarTexture));
			for (int i = 0; i<hp; i++){
				game.batch.draw(heartTexture,game.viewport.getWorldWidth()/2 + (i*16*Constants.SCALE + 28 * Constants.SCALE - getWidth(healthBarTexture)/2), game.viewport.getWorldHeight() - 22*Constants.SCALE,
						getWidth(heartTexture), getHeight(heartTexture));
			}
		}
		if(note) {
			game.batch.draw(bigNote, 0, 0,
					getWidth(bigNote), getHeight(bigNote));
		}
		game.batch.end();
	}

	private void createTable() {
		table = new Table();
		//table.setDebug(true); // turn on all debug lines
		table.top();
		table.setFillParent(true);

		levelLabel = new Label("LevelName", new Label.LabelStyle(game.font, Color.WHITE));
		scoreLabel = new Label(String.format("Score: %03d", score), new Label.LabelStyle(game.font, Color.WHITE));

		table.add(levelLabel).expandX().padTop(14);
		table.add(scoreLabel).expandX().padTop(14);
	}

	public void changeLevelName(String name){
		levelLabel.setText(name);
	}

	public void addScore(int value) {
		score += value;
		scoreLabel.setText(String.format("Score: %03d", score));
	}
	
	public void updateLives(Player p) {
		if (p.mainPlayer){
			hp = p.getHealth();
		}
		else{
			hp2 = p.getHealth();
		}
	}

	private float getWidth(Texture texture) {
		return texture.getWidth() * Constants.SCALE;
	}

	private float getHeight(Texture texture) {
		return texture.getHeight() * Constants.SCALE;
	}
}
