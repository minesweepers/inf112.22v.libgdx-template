package inf112.minesweepers;

import gameObjects.gameActors.enemies.BossPig;
import gameObjects.gameActors.enemies.EnemyPig;
import gameObjects.gameActors.enemies.WingPig;
import gameObjects.gameActors.players.Player;
import gameObjects.items.*;
import gameScreens.GameScreen;
import mapObjects.Platform;
import helper.Constants;
import mapObjects.Door;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;

import gameObjects.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public record LevelBuilder(World world) {
    public final static ArrayList<Body> tileBodies = new ArrayList<>();

    public enum Layers {
        DOOR("doorObjects", Door.class),
        PLATFORM("platformObjects", Platform.class),
        PLAYER("playerObjects", Player.class),
        ENEMY("enemyObjects", EnemyPig.class),
        BOSS("bossObjects", BossPig.class),
        DIAMOND("diamondObjects", Diamond.class),
        HEART("heartObjects", Heart.class),
        WING("wingEnemyObjects", WingPig.class),
        BOMB("bombObjects", Bomb.class),
        CROWN("crownObjects", Crown.class),
        BOX("boxObjects", Box.class),
        CANNON("cannonObjects", Cannon.class),
        NOTE("noteObjects", Note.class);

        public final String layerName;
        public final Class<? extends IGameObject> c;

        Layers(String layerName, Class<? extends IGameObject> c) {
            this.layerName = layerName;
            this.c = c;
        }
    }

    public void spawnPlayers(TiledMap map, ArrayList<GameObject> thingsToRender) {
        Array<Vector2> spawnPos = getSpawnPositions(map, Layers.PLAYER);

        for (GameObject gameObject : thingsToRender) {
            if (gameObject instanceof Player player) {
                Vector2 position = spawnPos.pop();
                scale(position);

                player.setPosition(position, 0);
            }
        }
    }

    /**
     * Spawn in every object in Layers except PLATFORM,DOOR and PLAYER
     */
    public void spawnGameObjects(TiledMap map, ArrayList<GameObject> thingsToRender) {
        for (int i = 3; i < Layers.values().length; i++) {
            addGameObjectToMap(thingsToRender, map, Layers.values()[i]);
        }
    }

    private Constructor<GameObject> getConstructor(Class<GameObject> c) throws NoSuchMethodException {
        return c.getConstructor(Vector2.class, World.class);
    }

    private void addGameObjectToMap(ArrayList<GameObject> thingsToRender, TiledMap map, Layers layer) {
        Array<Vector2> locations = getSpawnPositions(map, layer);

        Constructor<GameObject> cons;
        try {
            if (GameObject.class.isAssignableFrom(layer.c)) {
                cons = getConstructor((Class<GameObject>) layer.c);
            } else {
                return;
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            System.out.println("No constructor found for Object: " + layer.c);
            return;
        }

        for (Vector2 loc : locations) {
            try {
                scale(loc);
                thingsToRender.add(cons.newInstance(loc, world));
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                System.out.println("LevelBuilder::addGameObjectToMap\n" + e);
            }
        }
    }

    private void scale(Vector2 vector2) {
        vector2.x *= Constants.SCALE;
        vector2.y *= Constants.SCALE;
    }

    /**
     * Create hit-boxes/fixture for every tile in the platformsObjects and doorObjects layer
     */
    public void buildTiledMap(TiledMap map) {
        buildPolygons(map, Layers.PLATFORM);
        buildPolygons(map, Layers.DOOR);
    }

    private void buildPolygons(TiledMap map, Layers layer) {
        MapLayer platformObjectsLayer = map.getLayers().get(layer.layerName);
        if (platformObjectsLayer == null) {
            System.out.println("Could not load layer: " + layer.layerName);
            return;
        }

        for (MapObject obj : platformObjectsLayer.getObjects()) {
            if (obj instanceof PolygonMapObject tile) {
                if (layer == Layers.PLATFORM) {
                    tileBodies.add(new Platform(world, tile).getBody());
                } else if (layer == Layers.DOOR) {
                    tileBodies.add(new Door(world, tile).getBody());
                }
            }
        }
    }

    public Array<Vector2> getSpawnPositions(TiledMap map, Layers layerType) {
        Array<Vector2> spawnPositions = new Array<>();
        getObjectsPositions(map, spawnPositions, layerType.layerName);
        return spawnPositions;
    }

    private void getObjectsPositions(TiledMap map, Array<Vector2> array, String layerName) {
        MapLayer platformObjectsLayer = map.getLayers().get(layerName);
        if (platformObjectsLayer != null) {
            for (MapObject obj : platformObjectsLayer.getObjects()) {
                float x = obj.getProperties().get("x", Float.class);
                float y = obj.getProperties().get("y", Float.class);

                Vector2 objPos = new Vector2(x, y);

                array.add(objPos);
            }
        }
    }

    public void destroyTileBodies() {
        for (Body tileBody : tileBodies) {
            GameScreen.bodiesToDestroy.add(tileBody);
        }
        tileBodies.clear();
    }
}
