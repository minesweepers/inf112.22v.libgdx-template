package inf112.minesweepers;

import com.badlogic.gdx.Input;

public class ControlLayout {
	public int l;
	public int r;
	public int jump;
	public int attack;
	public int door;
	
	public ControlLayout(){  //default control scheme
		this.r = Input.Keys.RIGHT;
		this.l = Input.Keys.LEFT;
		this.jump = Input.Keys.UP;
		this.attack = Input.Keys.DOWN;
		this.door = Input.Keys.SPACE;
	}

	public void changeControls (int right, int left, int jump, int attack){  //custom controls, probs useful later
		this.r = right;
		this.l = left;
		this.jump = jump;
		this.attack = attack;
	}
}
