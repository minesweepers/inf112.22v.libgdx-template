package inf112.minesweepers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import gameScreens.MainMenuScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import gameScreens.SettingsScreen;
import helper.Constants;

public class Pigsweepers extends Game {
	public SpriteBatch batch;
	public StretchViewport viewport;
	public OrthographicCamera camera;
	public Preferences save;
	public Preferences settings;
	public Preferences stats;

	public boolean isPaused = false;
	public boolean mpOn;
	public boolean debug;
	public Stage pausedStage;
	public BitmapFont font;

	public FreeTypeFontGenerator generator;

	@Override
	public void create() {
		camera = new OrthographicCamera();
		viewport = new StretchViewport(1024*Constants.SCALE, 512*Constants.SCALE, camera);
		batch = new SpriteBatch();
		save = Gdx.app.getPreferences("PigsweepersSave");
		settings = Gdx.app.getPreferences("PigsweepersSettings");
		stats = Gdx.app.getPreferences("PigsweepersStats");
		generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/NorseBold.ttf"));

		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 30;
		parameter.borderWidth = 2;
		font = generator.generateFont(parameter);

		SettingsScreen settingsScreen = new SettingsScreen(this);
		settingsScreen.startUp();

		createPauseScreen();
		this.setScreen(new MainMenuScreen(this));
	}

	@Override
	public void render() {
		camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
		camera.update();
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		super.render();
	}

	@Override
	public void dispose() {
		batch.dispose();
		generator.dispose();
	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width,height);
		viewport.apply();
		createPauseScreen();
		super.resize(width,height);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	private void createPauseScreen(){
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		pausedStage = new Stage(new FitViewport(width,height), batch);

		Table table = createTable(height);

		pausedStage.addActor(table);
	}

	private Table createTable(int height) {
		Pigsweepers game = this;

		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 50;
		parameter.borderWidth = 2;
		BitmapFont pauseFont = generator.generateFont(parameter);

		Label.LabelStyle labelStyle = new Label.LabelStyle(pauseFont, Color.WHITE);
		TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();
		buttonStyle.font = font;

		Table table = new Table();
		//table.setDebug(debug); // turn on all debug lines
		table.center();
		table.setFillParent(true);

		Label pausedLabel = new Label("Paused", labelStyle);

		TextButton resumeButton = new TextButton("Resume", buttonStyle);
		resumeButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				if (isPaused){
					isPaused = false;
				}
			}
		});

		TextButton mainMenuButton = new TextButton("Main Menu", buttonStyle);
		mainMenuButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				if (isPaused){
					isPaused = false;
					setScreen(new MainMenuScreen(game));
					createPauseScreen();
				}
			}
		});

		table.add(pausedLabel).expandY().padBottom(10f).padTop(height/5f);
		table.row();
		table.add(resumeButton).expandY().padBottom(10f);
		table.row();
		table.add(mainMenuButton).expandY().padBottom(height/3f);

		return table;
	}
}