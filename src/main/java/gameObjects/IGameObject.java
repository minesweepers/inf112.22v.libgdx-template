package gameObjects;

import com.badlogic.gdx.physics.box2d.Body;

public interface IGameObject {

	Body getBody();
}
