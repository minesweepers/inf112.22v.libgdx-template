package gameObjects;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public abstract class GameObject implements IGameObject {

    public boolean isDeleted = false;

    public Vector2 getPosition(){
        return getBody().getPosition();
    }

    public void setPosition(Vector2 position, float angle){
        getBody().setTransform(position,angle);
    }

    public void flipTexture(Animation<TextureRegion> animation){
        if (animation != null){
            for (TextureRegion textures : animation.getKeyFrames()) {
                textures.flip(true,false);
            }
        }
    }

    abstract public Body getBody();

    public abstract TextureRegion getCurrentAnimation();
}
