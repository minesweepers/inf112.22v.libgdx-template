package gameObjects.items;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import gameObjects.gameActors.players.Player;
import gameObjects.gameActors.sensors.ExplosionSensor;
import gameScreens.GameScreen;
import helper.AudioHandler;
import helper.HelperClass;

public class Bomb extends Item {
    private final World world;
    private final Vector2 spawnPos;
    private final Body body;

    private Animation<TextureRegion> igniteAnimation;
    private Animation<TextureRegion> explosionAnimation;
    private Vector2 explosionPos;

    private ExplosionSensor explosionSensor;

    private boolean isIgnited = false; //Player has "lit" the bomb by hitting it with a hammer
    private boolean isExploded = false; // Bomb has finished exploding animation and is ready for another to spawn

    private float respawnTime = 0;
    private float igniteTime = 0;

    public Bomb(Vector2 pos, World world){
        super("Bomb Idle",1,1,1);
        this.world = world;
        this.spawnPos = pos;

        igniteAnimation();
        explosionAnimation();

        body = createBody(12, 12, pos, world);
    }

    @Override
    public Body getBody() {
        return body;
    }

    @Override
    public void pickUp(Player player) {
    }

    @Override
    public int getValue() {
        return 0;
    }

    @Override
    public void updateAnimation() {
        //During explosion animation
        if (getAnimation().equals(explosionAnimation)){
            if (explosionPos == null){
                explosionPos = getPosition();
            }
            else{
                setPosition(explosionPos,0);
            }
            //Explosion finished
            if (explosionAnimation.isAnimationFinished(stateTime)){
                if (explosionSensor != null){
                    explosionSensor.destroy();
                    explosionSensor = null;
                    isExploded = true;
                }
            }
            //Create a explosion sensor
            else if (!isExploded && explosionSensor == null){
                explosionSensor = new ExplosionSensor(this);
            }
        }

        if (isIgnited){
            //Respawn bomb
            if (isExploded){
                setPosition(spawnPos,0);

                respawnTime += Gdx.graphics.getDeltaTime();
                if (respawnTime >= 1){
                    GameScreen.addToRender.add(new Bomb(spawnPos, world));
                    isDeleted = true;
                    return;
                }
            }
            //Step forward ignite timer
            else{
                igniteTime += Gdx.graphics.getDeltaTime();
                if (igniteTime >= 5){
                    explode();
                }
            }
        }

        //Move sensor to bomb pos
        if (explosionSensor != null){
            explosionSensor.setPosition(getPosition(), 0);
        }
        super.updateAnimation();
    }

    public void ignite(){
        if (isExploded){
            return;
        }
        isIgnited = true;
        isLooping = true;
        setAnimation(igniteAnimation);
    }

    public boolean isExploded() {
        return isExploded;
    }

    public boolean isIgnited() {
        return isIgnited;
    }

    public void explode(){
        if (isExploded || !isLooping){
            return;
        }
        isLooping = false;
        setAnimation(explosionAnimation);
        //GameScreen.bodiesToDestroy.add(getBody());
        AudioHandler.playExplosion();
    }

    private void igniteAnimation() {
        Texture idleTexture = new Texture("itemAnimations/Bomb Explode.png");
        igniteAnimation = HelperClass.animationGen(idleTexture, 4, 1, 0.2f);
    }

    private void explosionAnimation() {
        Texture idleTexture = new Texture("itemAnimations/Bomb Explosion.png");
        explosionAnimation = HelperClass.animationGen(idleTexture, 6, 1, 0.1f);
    }
}
