package gameObjects.items;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.World;
import gameObjects.gameActors.players.Player;
import gameScreens.GameScreen;
import helper.AudioHandler;
import helper.HelperClass;
import gameObjects.items.CannonBall;

public class Cannon extends Item {
    private final World world;
    private final Body body;

    private Animation<TextureRegion> shootAnimation;
    private float shootTime = 0;
    private boolean shooting;

    public Cannon(Vector2 pos, World world){
        super("Cannon Idle",1,1,1);
        this.world = world;

        shootAnimation();

        body = createBody(12, 12, pos, world);
        MassData m = body.getMassData();
        m.mass = 5f;
        body.setMassData(m);
    }

    @Override
    public Body getBody() {
        return body;
    }

    @Override
    public void pickUp(Player player) {

    }

    @Override
    public int getValue() {
        return 0;
    }

    @Override
    public void updateAnimation() {
    	shootTime += Gdx.graphics.getDeltaTime();
    	if (shootTime>3f && !shooting) {
    		setAnimation(shootAnimation);
    		shooting=true;	
    	}
    	else {
    		if(shootTime>3.2f) {
    			Vector2 position=body.getPosition();
    			GameScreen.addToRender.add(new CannonBall(new Vector2(position.x-0.3f, position.y+0.05f), world));
        		shootTime=0;
                AudioHandler.playExplosion();
    		}
    		else if(shootTime>0.6f&&shootTime<2f) {
    			setAnimation(idleAnimation);
    			shooting=false;
    		}
    	}
        super.updateAnimation();
    }

   
    private void shootAnimation() {
        Texture idleTexture = new Texture("itemAnimations/Cannon Shoot.png");
        shootAnimation = HelperClass.animationGen(idleTexture, 4, 1, 0.2f);
    }

    public void destroy() {
        isDeleted = true;
    }
}
