package gameObjects.items;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.World;
import gameObjects.gameActors.players.Player;

public class Box extends Item{
    private final Body body;

    public Box(Vector2 pos, World world){
        super("Box Idle",0.2f,1,1);
        body = createBody(14, 14, pos, world);
        MassData m = body.getMassData();
        m.mass = 10f;
        body.setMassData(m);
    }

    @Override
    public Body getBody() {
        return body;
    }

    @Override
    public void pickUp(Player player) {

    }

    @Override
    public int getValue() {
        return 0;
    }

    public void destroy() {
        //GameScreen.bodiesToDestroy.add(getBody());
        isDeleted = true;
    }
}
