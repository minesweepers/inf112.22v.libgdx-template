package gameObjects.items;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import gameObjects.gameActors.players.Player;
import gameScreens.GameScreen;
import inf112.minesweepers.Hud;

public class Diamond extends Item {
    private final int value = 10;
    private final Body body;

    public Diamond(Vector2 position, World world){
        super("Big Diamond Idle");
        body = createBody(position, world);
    }

    @Override
    public int getValue(){
        return value;
    }

    @Override
    public void pickUp(Player player) {
        isDeleted = true;
    	if (player != null){
            Hud.diamonds++;
    		GameScreen.hud.addScore(getValue());
    	}
    }

    @Override
    public Body getBody() {
        return body;
    }
}

