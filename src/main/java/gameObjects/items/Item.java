package gameObjects.items;

import gameObjects.GameObject;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import gameObjects.gameActors.players.Player;
import helper.Constants;
import helper.HelperClass;

public abstract class Item extends GameObject {
    private TextureRegion currentAnimation;
    private Animation<TextureRegion> animation;

    private final String fileName;
    private final float animationFrameDuration;
    private final int IDLE_FRAME_COLS;
    private final int IDLE_FRAME_ROWS;

    protected float stateTime;
    protected boolean isLooping = true;
    protected Animation<TextureRegion> idleAnimation;

    Item(String idleAnimationFileName){
        this.fileName = idleAnimationFileName;
        this.animationFrameDuration = 0.2f;
        this.IDLE_FRAME_COLS = 10;
        this.IDLE_FRAME_ROWS = 1;
        idleAnimation();
    }

    Item(String idleAnimationFileName, float animationFrameDuration, int IDLE_FRAME_COLS, int IDLE_FRAME_ROWS){
        this.fileName = idleAnimationFileName;
        this.animationFrameDuration = animationFrameDuration;
        this.IDLE_FRAME_COLS = IDLE_FRAME_COLS;
        this.IDLE_FRAME_ROWS = IDLE_FRAME_ROWS;
        idleAnimation();
    }

    public void idleAnimation() {
        Texture idleTexture = new Texture("itemAnimations/"+fileName+".png");
        idleAnimation = HelperClass.animationGen(idleTexture, IDLE_FRAME_COLS, IDLE_FRAME_ROWS, animationFrameDuration);
        setAnimation(idleAnimation);
    }

    public TextureRegion getCurrentAnimation(){
        return currentAnimation;
    }

    public void setAnimation(Animation<TextureRegion> animation){
        stateTime = 0;
        this.animation = animation;
    }

    public Animation<TextureRegion> getAnimation() {
        return animation;
    }

    public void updateAnimation(){
        stateTime += Gdx.graphics.getDeltaTime();
        currentAnimation = animation.getKeyFrame(stateTime,isLooping);
    }

    public Body createBody(Vector2 position, World world){
        return createBody(10, 10, position, world);
    }

    public Body createBody(int hx, int hy,Vector2 position, World world) {
        //Create a physics body
        BodyDef bodyDef = new BodyDef();
        bodyDef.type =  BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(position);

        Body body = world.createBody(bodyDef);
        body.setUserData(this);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(hx * Constants.SCALE, hy * Constants.SCALE);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        body.createFixture(fixtureDef);

        shape.dispose();
        return body;
    }

    public abstract void pickUp(Player player);

    public abstract int getValue();
}
