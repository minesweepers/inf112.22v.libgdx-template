package gameObjects.items;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import gameObjects.gameActors.players.Player;

/**
 * Used as a "key" for some doors, to get it either defeat a boss or find it somewhere
 * */
public class Crown extends Item{
    private final Body body;
    public boolean isBossCrown = false; //If the crown is dropped by the bossPig

    public Crown(Vector2 pos, World world){
        super("Crown Idle",0.2f,10,1);
        body = createBody(12,5,pos, world);
    }

    @Override
    public Body getBody() {
        return body;
    }

    @Override
    public void pickUp(Player player) {
        isDeleted = true;
        if (player != null){
            if (isBossCrown){
                player.equipCrown();
            }
            player.hasCrown = true;
        }
    }

    @Override
    public int getValue() {
        return 0;
    }
}
