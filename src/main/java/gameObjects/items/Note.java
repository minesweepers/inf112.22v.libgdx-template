package gameObjects.items;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import gameObjects.gameActors.players.Player;

public class Note extends Item {
    private final int value = 0;
    private final Body body;

    public Note(Vector2 position, World world){
    	super("Note",1,1,1);
        body = createBody(16,16,position, world);
    }

    @Override
    public int getValue(){
        return value;
    }

    @Override
    public void pickUp(Player player) {
    	if (player != null){
    		isDeleted = true;
    	}
    }

    @Override
    public Body getBody() {
        return body;
    }
}
