package gameObjects.items;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import gameObjects.gameActors.players.Player;
import gameScreens.GameScreen;

public class Heart extends Item {
    private final int value = 50;
    private final Body body;

    public Heart(Vector2 position, World world){
        super("Big Heart Idle");
        body = createBody(position, world);

        //make heavy so pigs can't move it
        MassData massData = body.getMassData();
        massData.mass = 4;
        body.setMassData(massData);
    }

    @Override
    public int getValue(){
        return value;
    }

    @Override
    public void pickUp(Player player) {
        isDeleted = true;
        if (player != null){
            if (player.getHealth() >= 5) {
                GameScreen.hud.addScore(getValue());
            } else {
                player.setHealth(player.getHealth() + 1);
                GameScreen.hud.updateLives(player);
            }
    	}
    }

    @Override
    public Body getBody() {
        return body;
    }
}

