package gameObjects.items;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import gameObjects.gameActors.players.Player;

public class CannonBall extends Item {
    private final int value = 0;
    private final Body body;

    public CannonBall(Vector2 position, World world){
        super("Cannon Ball",1,1,1);
        body = createBody(position, world);
        body.setBullet(true);
        body.setGravityScale(0);
        body.setLinearVelocity(-3f,0);
    }

    @Override
    public int getValue(){
        return value;
    }

    @Override
    public void pickUp(Player player) {
    }
    
    public void destroy() {
        isDeleted = true;
    }
    
    @Override
    public void updateAnimation() {
    	if (body.getLinearVelocity().x<2f && body.getLinearVelocity().x>-2) {
    		isDeleted=true;	
    	}
        super.updateAnimation();
    }

    @Override
    public Body getBody() {
        return body;
    }
}

