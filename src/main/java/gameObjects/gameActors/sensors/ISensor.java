package gameObjects.gameActors.sensors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public interface ISensor {

    void setPosition(Vector2 position, float angle);

    Body getBody();

    void destroy();
}
