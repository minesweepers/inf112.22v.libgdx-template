package gameObjects.gameActors.sensors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import gameObjects.items.Bomb;
import gameScreens.GameScreen;
import helper.Constants;

public class ExplosionSensor implements ISensor {
    private final World world;
    private final Body body;
    private final Bomb bomb;

    public final float sensorRadius = 60;


    public ExplosionSensor(Bomb bomb){
        this.world = bomb.getBody().getWorld();
        this.bomb = bomb;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.gravityScale = 0.01f; // Fix for not detecting collisions
        bodyDef.position.set(bomb.getPosition());

        body = world.createBody(bodyDef);
        body.setUserData(this);

        CircleShape shape = new CircleShape();
        shape.setRadius(sensorRadius * Constants.SCALE);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef);
        shape.dispose();
    }

    public Bomb getBomb() {
        return bomb;
    }

    @Override
    public void setPosition(Vector2 position, float angle) {
        body.setTransform(position,angle);
    }

    @Override
    public Body getBody() {
        return body;
    }

    @Override
    public void destroy() {
        GameScreen.bodiesToDestroy.add(getBody());
    }
}
