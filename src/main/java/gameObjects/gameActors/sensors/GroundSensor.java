package gameObjects.gameActors.sensors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import gameObjects.gameActors.GameActor;
import gameScreens.GameScreen;
import helper.Constants;

public class GroundSensor implements ISensor {
    private final GameActor gameActor;
    private final World world;
    private Body body;

    private int offset = 18;

    public GroundSensor(GameActor gameActor){
        this.gameActor = gameActor;
        this.world = gameActor.getBody().getWorld();

        createBody();
    }

    public GroundSensor(GameActor gameActor, int offset){
        this.gameActor = gameActor;
        this.world = gameActor.getBody().getWorld();
        this.offset = offset;

        createBody();
    }

    private void createBody() {
        //Create a physics body
        BodyDef bodyDef = new BodyDef();
        bodyDef.type =  BodyDef.BodyType.DynamicBody;
        bodyDef.gravityScale = 0.01f; // Fix for not detecting collisions

        Vector2 position = gameActor.getPosition();
        position.y -= offset * Constants.SCALE;

        bodyDef.position.set(position);
        body = world.createBody(bodyDef);
        body.setUserData(this);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(((gameActor.sprite.getWidth() - 10) / 4) * Constants.SCALE, 2 * Constants.SCALE);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.isSensor = true;
        fixtureDef.shape = shape;
        body.createFixture(fixtureDef);
    }

    public GameActor getGameActor() {
        return gameActor;
    }

    @Override
    public void setPosition(Vector2 position, float angle) {
        body.setLinearVelocity(new Vector2(0,-0.05f));
        position.y -= offset * Constants.SCALE;
        body.setTransform(position,0);
    }

    @Override
    public Body getBody() {
        return body;
    }

    @Override
    public void destroy() {
    	GameScreen.bodiesToDestroy.add(body);
    }
}