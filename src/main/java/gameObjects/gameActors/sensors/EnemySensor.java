package gameObjects.gameActors.sensors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import gameObjects.gameActors.enemies.Enemy;
import gameScreens.GameScreen;
import helper.Constants;

public class EnemySensor implements ISensor {
    private final SensorType sensorType;
    private final Body body;
    private final World world;
    private final Enemy enemy;

    private boolean runningLeft;

    public final float radiusOfSensor;
    public float sensorOffSet;

    /**
     * TURN_SENSOR: Turns when in radius
     * ATTACK_SENSOR: Attacks when in radius
     * */
    public enum SensorType{
        TURN_SENSOR,
        ATTACK_SENSOR
    }

    public EnemySensor(Enemy enemy, SensorType sensorType, int radius, int sensorOffSet) {
        this.world = enemy.getBody().getWorld();
        this.enemy = enemy;
        this.sensorType = sensorType;
        this.radiusOfSensor = radius;
        this.sensorOffSet = sensorOffSet;
        this.runningLeft = enemy.isLookingLeft;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.gravityScale = 0.01f; // Fix for not detecting collisions

        body = world.createBody(bodyDef);
        body.setUserData(this);

        CircleShape shape = new CircleShape();
        shape.setRadius(radiusOfSensor * Constants.SCALE);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef);
        shape.dispose();
    }

    public SensorType getSensorType() {
        return sensorType;
    }

    public Enemy getEnemy() {
        return enemy;
    }

    @Override
    public void setPosition(Vector2 position, float angle) {
        if (enemy.isLookingLeft != this.runningLeft){
            sensorOffSet = -sensorOffSet;
            this.runningLeft = enemy.isLookingLeft;
        }
        body.setTransform(new Vector2(position.x-sensorOffSet*Constants.SCALE, position.y),angle);
    }

    @Override
    public Body getBody() {
        return body;
    }

    public void turnEnemy(){
        enemy.turn();
    }

    public void setAtkForEnemy(){
        enemy.isAttacking = true;
    }

    @Override
    public void destroy() {
    	GameScreen.bodiesToDestroy.add(body);
        //world.destroyBody(body);
    }
}
