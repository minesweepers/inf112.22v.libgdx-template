package gameObjects.gameActors.sensors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import gameObjects.gameActors.GameActor;
import gameScreens.GameScreen;
import helper.AudioHandler;
import helper.Constants;

public class AttackHitSensor implements ISensor {
    private final Body body;
    private final World world;
    private final GameActor gameActor;
    private final int damage;

    private boolean isLookingLeft;
    private float atkOffSetX;
    private float atkOffSetY;

    public AttackHitSensor(GameActor gameActor, int offsetX, int offsetY, int boxSizeX, int boxSizeY, int damage){
        this.world = gameActor.getBody().getWorld();
        this.gameActor = gameActor;
        this.atkOffSetX = offsetX * Constants.SCALE;
        this.atkOffSetY = offsetY * Constants.SCALE;
        this.damage = damage;
        this.isLookingLeft = gameActor.isLookingLeft;

        if (isLookingLeft) {
            atkOffSetX = -atkOffSetX;
        }

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.gravityScale = 0.01f;

        body = world.createBody(bodyDef);
        body.setUserData(this);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(boxSizeX * Constants.SCALE, boxSizeY * Constants.SCALE);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.isSensor = true;

        body.createFixture(fixtureDef);
        shape.dispose();

        AudioHandler.playAttack();
    }

    public int getDamage() {
        return damage;
    }

    public float getAtkOffSetX() {
        return atkOffSetX;
    }

    public GameActor getGameActor() {
        return gameActor;
    }

    @Override
    public void setPosition(Vector2 position, float angle) {
        if (gameActor.isLookingLeft != this.isLookingLeft){
            atkOffSetX = -atkOffSetX;
            this.isLookingLeft = gameActor.isLookingLeft;
        }
        body.setTransform(new Vector2(position.x + atkOffSetX, position.y + atkOffSetY),0);
    }

    @Override
    public Body getBody() {
        return body;
    }

    @Override
    public void destroy() {
    	GameScreen.bodiesToDestroy.add(body);
    }
}
