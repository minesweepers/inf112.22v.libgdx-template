package gameObjects.gameActors.enemies;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

import gameObjects.gameActors.sensors.AttackHitSensor;
import gameObjects.gameActors.sensors.EnemySensor;
import helper.Constants;

public class EnemyPig extends Enemy {
    private float turnCoolDown = 0.0f;
    private final float turningThreshold = 0.015f; //If the movement threshold is not met the pig turns

    private EnemySensor enemySensor = null;
    private EnemySensor enemyTurnSensor = null;

    private final int radForHitSensor = 25;
    private final int radForTurnSensor = 75;

    private final Body body;
    private AttackHitSensor enemyAttackHitSensor = null;

    public EnemyPig(Vector2 position, World world){
        super(0.2f,11,1,6,1,5,1,4,2);
        
        Random rnd=new Random();
        int pigtype=rnd.nextInt(5);
        if(pigtype<=1) {
        	setHealth(6);
        	setup("tankPigAnimations");
        }
        else if(pigtype==2) {
            setHealth(2);
        	setMaxSpeed(getMaxSpeed()*2);
        	setMovementSpeed(getMovementSpeed()*2);
        	setup("weebPigAnimations");
        }
        else {
        	setHealth(4);
        	setup("pigAnimations");
        }

        BodyDef bodyDef = new BodyDef();
        bodyDef.type =  BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(position);

        body = world.createBody(bodyDef);
        body.setUserData(this);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox((texture.getWidth()/2.7f) * Constants.SCALE, (texture.getHeight()/2.7f) * Constants.SCALE);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        body.createFixture(fixtureDef);

        shape.dispose();
        makeSensor();
    }

    @Override
    public Body getBody() {
        return body;
    }

    public void makeSensor(){
        this.enemySensor = new EnemySensor(this, EnemySensor.SensorType.ATTACK_SENSOR, radForHitSensor, 10);
        this.enemyTurnSensor = new EnemySensor(this, EnemySensor.SensorType.TURN_SENSOR,radForTurnSensor,0);
    }

    public void move() {
        setAtkHitBoxPosition();
        if (enemySensor != null){
            enemySensor.setPosition(this.getPosition(),0);
        }
        if (enemyTurnSensor != null){
            enemyTurnSensor.setPosition(this.getPosition(),0);
        }

        if (isDead){
            return;
        }

    	float deltaTime = Gdx.graphics.getDeltaTime();
    	Vector2 linearVelocity = body.getLinearVelocity();
    	if (turnCoolDown >= 0) {
    		turnCoolDown -= deltaTime;
    	}

        //Prevent walking off screen
        if (getPosition().x < 0 && !isLookingLeft){
            setPosition(new Vector2(0, getPosition().y),0);
            turn();
        }
        else if (getPosition().x > Constants.SCREEN_EDGE && isLookingLeft) {
            setPosition(new Vector2(Constants.SCREEN_EDGE, getPosition().y),0);
        	turn();
        }
        else if (Math.abs(linearVelocity.x) <= turningThreshold && turnCoolDown <= 0){
            turn();
        }
        //Switch direction when hitting obstacle
        //Use a turn cooldown to prevent jittering when turning
        //Does not use this on walking off screen check to prevent pigs from falling off
        //Prevent pig from being shoved off screen by other pigs
        linearVelocity.x += getMovementSpeed() * deltaTime;
        linearVelocity.x = MathUtils.clamp(linearVelocity.x, -getMaxSpeed(), getMaxSpeed());
        body.setLinearVelocity(linearVelocity);

    }
    @Override
    public void setAtkAnimation() {
        stateTimeAtkAndDeath += Gdx.graphics.getDeltaTime();
        if (!atkAnimation.isAnimationFinished(stateTimeAtkAndDeath)) {
            isAttacking = true;
            setCurrentAnimation(atkAnimation.getKeyFrame(stateTimeAtkAndDeath));
        } else {
            isAttacking = false;
            updateAnimation();
        }
    }
    public void setAtkHitBoxPosition(){
        if (enemyAttackHitSensor != null){
            enemyAttackHitSensor.setPosition(this.getPosition(),0);
        }
    }

    @Override
    public void turn(){
        turnCoolDown = 0.5f;
        super.turn();
    }

    public void atkHitBox() {
        if (!hasAtkHitBox) {
            enemyAttackHitSensor = new AttackHitSensor(this, 22,5,3,8,1);
            setAtkHitBoxPosition();
            hasAtkHitBox = true;
        }
    }

    public void deleteHitBox() {
        if (enemyAttackHitSensor != null) {
            enemyAttackHitSensor.destroy();
            enemyAttackHitSensor = null;
        }
    }

    public AttackHitSensor getEnemyAttackHitBox(){
        return enemyAttackHitSensor;
    }

    @Override
    public void die() {
        isDeleted = true;
        enemySensor.destroy();
        enemyTurnSensor.destroy();
        if (enemyAttackHitSensor != null) enemyAttackHitSensor.destroy();
    }

    @Override
    public int getValue() {
        return 5;
    }
}
