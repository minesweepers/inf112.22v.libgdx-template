package gameObjects.gameActors.enemies;

import gameObjects.gameActors.GameActor;
import gameObjects.gameActors.sensors.AttackHitSensor;

public abstract class Enemy extends GameActor {
    private float movementSpeed = -5f;
    private float maxSpeed = 0.5f;

    protected boolean hasAtkHitBox = false;

    public boolean isAttacking;

    Enemy(String folderName, float animationFrameDuration, int IDLE_FRAME_COLS, int IDLE_FRAME_ROWS, int RUN_FRAME_COLS, int RUN_FRAME_ROWS, int ATK_FRAME_COLS, int ATK_FRAME_ROWS, int DEATH_FRAME_COLS, int HIT_FRAME_COLS) {
        super(animationFrameDuration, IDLE_FRAME_COLS, IDLE_FRAME_ROWS, RUN_FRAME_COLS, RUN_FRAME_ROWS, ATK_FRAME_COLS, ATK_FRAME_ROWS, DEATH_FRAME_COLS, HIT_FRAME_COLS);
        super.setup(folderName);
    }
    
    Enemy(float animationFrameDuration, int IDLE_FRAME_COLS, int IDLE_FRAME_ROWS, int RUN_FRAME_COLS, int RUN_FRAME_ROWS, int ATK_FRAME_COLS, int ATK_FRAME_ROWS, int DEATH_FRAME_COLS, int HIT_FRAME_COLS) {
        super(animationFrameDuration, IDLE_FRAME_COLS, IDLE_FRAME_ROWS, RUN_FRAME_COLS, RUN_FRAME_ROWS, ATK_FRAME_COLS, ATK_FRAME_ROWS, DEATH_FRAME_COLS, HIT_FRAME_COLS);
    }

    public abstract int getValue();

    public void setMovementSpeed(float movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public float getMovementSpeed() {
        return movementSpeed;
    }

    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public float getMaxSpeed() {
        return maxSpeed;
    }

    public void turn(){
        setMovementSpeed(-getMovementSpeed());
        flipTexture();
        isLookingLeft = !isLookingLeft;
    }

    public abstract void move();

    public abstract void deleteHitBox();

    public abstract void atkHitBox();

    public boolean hasAtkHitBox(){
        return hasAtkHitBox;
    }

    public void setHasAtkHitBox(boolean b){
        hasAtkHitBox = b;
    }

    public abstract AttackHitSensor getEnemyAttackHitBox();
}
