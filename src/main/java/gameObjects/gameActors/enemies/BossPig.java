package gameObjects.gameActors.enemies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import gameObjects.gameActors.sensors.AttackHitSensor;
import gameObjects.gameActors.sensors.EnemySensor;
import gameObjects.gameActors.sensors.GroundSensor;
import gameObjects.items.Crown;
import gameScreens.GameScreen;
import helper.Constants;

public class BossPig extends Enemy {
    private float turnCoolDown = 0.0f;
    private final float turningThreshold = 0.025f; //If the movement threshold is not met the pig turns

    private EnemySensor enemySensor = null;
    private EnemySensor enemyTurnSensor = null;
    private GroundSensor groundSensor = null;

    private final int radForHitSensor = 30;
    private final int radForTurnSensor = 100;

    private final Body body;
    private final World world;
    private AttackHitSensor enemyAttackHitSensor = null;

    public float panicTimer = 0;
    private float jumpCooldown = 0;
    private boolean hasSpawnedCrown = false;

    public BossPig(Vector2 position, World world) {
        super("BigPigAnimations", 0.2f, 11, 1, 6, 1, 5, 1, 4, 2);
        this.world = world;
        setHealth(10);

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(position);

        body = world.createBody(bodyDef);
        body.setUserData(this);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox((texture.getWidth()/1.8f) * Constants.SCALE,(texture.getHeight()/1.3f) * Constants.SCALE);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        body.createFixture(fixtureDef);

        //Make heavy enough so that a player can not make it turn by pushing it
        MassData massData = body.getMassData();
        massData.mass = 5;
        body.setMassData(massData);

        shape.dispose();
        makeSensor();
    }

    @Override
    public Body getBody() {
        return body;
    }

    public void makeSensor() {
        this.groundSensor = new GroundSensor(this, 26);
        this.enemySensor = new EnemySensor(this, EnemySensor.SensorType.ATTACK_SENSOR, radForHitSensor, 10);
        this.enemyTurnSensor = new EnemySensor(this, EnemySensor.SensorType.TURN_SENSOR, radForTurnSensor, 0);
    }

    @Override
    public void move() {
        float deltaTime = Gdx.graphics.getDeltaTime();
        Vector2 linearVelocity = body.getLinearVelocity();
        jumpCooldown -= deltaTime;

        setAtkHitBoxPosition();
        if (enemySensor != null) {
            enemySensor.setPosition(this.getPosition(), 0);
        }
        if (enemyTurnSensor != null) {
            enemyTurnSensor.setPosition(this.getPosition(), 0);
        }

        if (isDead) {
            if (!hasSpawnedCrown){
                hasSpawnedCrown = true;
                Crown crown = new Crown(getPosition(), world);
                crown.isBossCrown = true;
                GameScreen.addToRender.add(crown);
            }
            return;
        }
        groundSensor.setPosition(getPosition(), 0);

        //When hitting an ignited bomb start to run and jump
        if (panicTimer > 0){
            panicTimer -= deltaTime;
            setMaxSpeed(2f);
        }
        else{
            setMaxSpeed(0.8f);
        }

        if (turnCoolDown >= 0) {
            turnCoolDown -= deltaTime;
        }

        //Prevent walking off screen
        if (getPosition().x < 0 && !isLookingLeft) {
            setPosition(new Vector2(0, getPosition().y), 0);
            turn();
        } else if (getPosition().x > Constants.SCREEN_EDGE && isLookingLeft) {
            setPosition(new Vector2(Constants.SCREEN_EDGE, getPosition().y), 0);
            turn();
        } else if (Math.abs(linearVelocity.x) <= turningThreshold && turnCoolDown <= 0 && hasGroundContact()) {
            turn();
        }

        linearVelocity.x += getMovementSpeed() * deltaTime;
        linearVelocity.x = MathUtils.clamp(linearVelocity.x, -getMaxSpeed(), getMaxSpeed());
        body.setLinearVelocity(linearVelocity);
    }

    public void jump(float multiplier) {
        if (jumpCooldown <= 0 && hasGroundContact()){
            body.setLinearVelocity(new Vector2(body.getLinearVelocity().x,0));
            body.applyLinearImpulse(new Vector2(0,13f * multiplier), getPosition(),true);
            jumpCooldown = 0.5f;
        }
    }

    public void panic() {
        panicTimer = 3;
        jumpCooldown = jumpCooldown/2;
        isAttacking = true;
    }

    @Override
    public void setAtkAnimation() {
        stateTimeAtkAndDeath += Gdx.graphics.getDeltaTime();
        if (!atkAnimation.isAnimationFinished(stateTimeAtkAndDeath)) {
            isAttacking = true;
            setCurrentAnimation(atkAnimation.getKeyFrame(stateTimeAtkAndDeath));
        }
        else{
            isAttacking = false;
            updateAnimation();
        }
    }

    private void setAtkHitBoxPosition() {
        if (enemyAttackHitSensor != null) {
            enemyAttackHitSensor.setPosition(this.getPosition(), 0);
        }
    }

    @Override
    public void turn() {
        turnCoolDown = 0.5f;
        super.turn();
    }

    public void atkHitBox() {
        if (!hasAtkHitBox) {
            enemyAttackHitSensor = new AttackHitSensor(this, 30, 6, 7, 18, 2);
            setAtkHitBoxPosition();
            hasAtkHitBox = true;
        }
    }

    public void deleteHitBox() {
        if (enemyAttackHitSensor != null) {
            enemyAttackHitSensor.destroy();
            enemyAttackHitSensor = null;
        }
    }

    public AttackHitSensor getEnemyAttackHitBox() {
        return enemyAttackHitSensor;
    }

    @Override
    public void die() {
        isDeleted = true;
        groundSensor.destroy();
        enemySensor.destroy();
        enemyTurnSensor.destroy();

        if (enemyAttackHitSensor != null) enemyAttackHitSensor.destroy();
    }

    @Override
    public int getValue() {
        return 50;
    }
}
