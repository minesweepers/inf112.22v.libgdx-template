package gameObjects.gameActors.enemies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

import gameObjects.gameActors.sensors.AttackHitSensor;
import helper.Constants;

public class WingPig extends Enemy {
    private float turnCoolDown = 0.0f;
    private final float turningThreshold = 0.015f; //If the movement threshold is not met the pig turns

    private final Body body;

    public WingPig(Vector2 position, World world){
        super("wingPigAnimations",0.2f,11,1,6,1,5,1,4,2);
        setHealth(3);

        BodyDef bodyDef = new BodyDef();
        bodyDef.type =  BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(position);

        body = world.createBody(bodyDef);
        body.setUserData(this);
        body.setGravityScale(0);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox((texture.getWidth()/2.7f) * Constants.SCALE, (texture.getHeight()/2.7f) * Constants.SCALE);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        body.createFixture(fixtureDef);

        shape.dispose();
    }

    @Override
    public Body getBody() {
        return body;
    }

    @Override
    public void move() {
        if (isDead){
            return;
        }
    	float deltaTime = Gdx.graphics.getDeltaTime();
    	Vector2 linearVelocity = body.getLinearVelocity();
    	
    	if (turnCoolDown >= 0) {
    		turnCoolDown -= deltaTime;
    	}
    	
        //Prevent walking off screen
        if (getPosition().x <= 0 && isLookingLeft){
            turn();
        }
        else if (getPosition().x >= Constants.SCREEN_EDGE && !isLookingLeft) {
        	turn();
        }
        //Switch direction when hitting obstacle
        //Use a turn cooldown to prevent jittering when turning
        //Does not use this on walking off screen check to prevent pigs from falling off
        else if (Math.abs(linearVelocity.x) <= turningThreshold && turnCoolDown <= 0){
            turn();
        }

        //Prevent pig from being shoved off screen by other pigs
        if (getPosition().x < 0){
            setPosition(new Vector2(0, getPosition().y),0);
        }
        else if (getPosition().x > Constants.SCREEN_EDGE){
            setPosition(new Vector2(Constants.SCREEN_EDGE, getPosition().y),0);
        }
        
        linearVelocity.x += getMovementSpeed() * deltaTime;
        linearVelocity.x = MathUtils.clamp(linearVelocity.x, -getMaxSpeed(), getMaxSpeed());
        body.setLinearVelocity(linearVelocity);
    }

    @Override
    public void deleteHitBox() {

    }

    @Override
    public void atkHitBox() {

    }

    @Override
    public boolean hasAtkHitBox() {
        return false;
    }

    @Override
    public void setHasAtkHitBox(boolean b) {

    }

    @Override
    public AttackHitSensor getEnemyAttackHitBox() {
        return null;
    }

    @Override
    public void turn(){
        setMovementSpeed(-getMovementSpeed());
        flipTexture();
        isLookingLeft = !isLookingLeft;
        turnCoolDown = 0.5f;
    }

    @Override
    public void die() {
        isDeleted = true;
    }

    @Override
    public int getValue() {
        return 5;
    }
}
