package gameObjects.gameActors.players;

import gameObjects.gameActors.GameActor;
import gameObjects.gameActors.sensors.AttackHitSensor;
import gameObjects.gameActors.sensors.GroundSensor;
import gameScreens.GameOverScreen;
import gameScreens.GameScreen;
import gameScreens.WinScreen;
import helper.AudioHandler;
import inf112.minesweepers.Hud;
import mapObjects.Door;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

import helper.Constants;
import inf112.minesweepers.ControlLayout;
import inf112.minesweepers.Pigsweepers;

public class Player extends GameActor {
    public final int JUMP_STRENGTH = 5;
    public final int MOVEMENT_FORCE = 5;
    public final float MOVEMENT_MAX = 2.4f;

    public boolean isAttacking = false;

    public boolean isStillAttacking = false;
    public boolean hasAtkHitBox = false;

    public final boolean mainPlayer;
    public boolean hasCrown = false;

    public final Pigsweepers game;

    private final Body body;
    private final ControlLayout controls;
    private final GroundSensor groundSensor;
    private AttackHitSensor playerWeaponHitBox = null;
    private Door door = null; // Saves the Door obj when player is in contact with it, null otherwise

    public Player(Vector2 position, World world, Boolean mainPlayer, Pigsweepers game) {
        super(0.2f, 11, 1, 8, 1, 3, 1, 4, 2);
        super.isLookingLeft = false;
        this.mainPlayer = mainPlayer;
        this.game = game;

        if (mainPlayer) {
            //If player has already won the game before then equip crown.
            if (game.stats.getBoolean("crown")) {
                equipCrown();
            } else {
                super.setup("playerAnimationsNoCrown");
            }

            //Update player health
            if (game.save.contains("playerHp")) {
                setHealth(game.save.getInteger("playerHp"));
            } else {
                super.setHealth(5);
            }
        } else {
            super.setup("player2Animations");
            if (game.save.contains("player2Hp")) {
                setHealth(game.save.getInteger("player2Hp"));
            } else {
                super.setHealth(5);
            }
        }
        super.sprite.setOriginCenter();

        // Create a physics body
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;

        bodyDef.position.set(position);
        body = world.createBody(bodyDef);
        body.setUserData(this);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(((texture.getWidth() / 4.0f) - 1) * Constants.SCALE, ((texture.getHeight() / 4.0f) - 1) * Constants.SCALE);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        body.createFixture(fixtureDef);

        // Add control scheme
        controls = new ControlLayout();
        groundSensor = new GroundSensor(this);

        shape.dispose();
    }

    public void movePlayer(Input input) {
        boolean right;
        boolean left;
        boolean jump;

        right = input.isKeyPressed(controls.r);
        left = input.isKeyPressed(controls.l);
        jump = input.isKeyJustPressed(controls.jump);
        isAttacking = input.isKeyPressed(controls.attack);

        float deltaTime = Gdx.graphics.getDeltaTime();
        Vector2 linearVelocity = body.getLinearVelocity();
        Vector2 position = getPosition();

        groundSensor.setPosition(position, 0);
        if (game.isPaused || isDead || GameScreen.hud.note) {
            return;
        }

        if (input.isKeyJustPressed(controls.door) && hasGroundContact()) {
            checkDoor();
        }

        if (right && linearVelocity.x < MOVEMENT_MAX) {
            if (linearVelocity.x < 0) {
                body.setLinearVelocity(0, linearVelocity.y);
            }

            body.applyLinearImpulse(new Vector2(MOVEMENT_FORCE * deltaTime, 0), position, true);
            //flipper alle frames av animasjon når du går den veien
            if (isLookingLeft) flipPlayer(true);
        }

        if (left && linearVelocity.x > -MOVEMENT_MAX) {
            if (linearVelocity.x > 0) { //cancel movement
                body.setLinearVelocity(0, linearVelocity.y);
            }

            body.applyLinearImpulse(new Vector2(-MOVEMENT_FORCE * deltaTime, 0), position, true);
            //flipper alle frames av animasjon når du går den veien
            if (!isLookingLeft) flipPlayer(false);
        }

        //Cancel movement in x-axis
        if (!left && !right) {
            if (hasGroundContact())
                body.setLinearVelocity(0, body.getLinearVelocity().y);
        }

        if (jump) {
            if (hasGroundContact()) {
                //Stop velocity in the y-axis, but keep the x velocity
                body.setLinearVelocity(new Vector2(linearVelocity.x, 0));

                //Add jump velocity
                body.applyLinearImpulse(new Vector2(0, JUMP_STRENGTH), position, true);

                AudioHandler.playJump();
            }
        }

        //Prevent player from going off screen
        if (getPosition().x < 0) {
            setPosition(new Vector2(0, getPosition().y), 0);
        } else if (getPosition().x > Constants.SCREEN_EDGE) {
            setPosition(new Vector2(Constants.SCREEN_EDGE, getPosition().y), 0);
        } else if (getPosition().y < -10) {
            die();
        }

        //Update hitbox position for attack
        if (playerWeaponHitBox != null) {
            playerWeaponHitBox.setPosition(getPosition(), 0);
        }
    }

    private void flipPlayer(boolean bool) {
        isLookingLeft = !bool;
        flipTexture();
    }

    private void checkDoor() {
        if (door != null) {
            if (door.isLocked()) {
                if (hasCrown) {
                    hasCrown = false;
                    door.isLocked(false);
                } else {
                    AudioHandler.playLockedDoor();
                    return;
                }
            }

            AudioHandler.playOpenDoor();
            if (door.getNumber() == null) {
                door.changeLevel();
            } else {
                //WinScreen is displayed if door number is 100
                if (door.getNumber() == 100) {
                    game.save.putInteger("diamonds", Hud.diamonds);
                    game.save.flush();
                    game.setScreen(new WinScreen(game));
                } else {
                    Vector2 loc = door.teleport();
                    if (loc != null) {
                        setPosition(loc, 0);
                    }
                }
            }
            door = null;
        }
    }

    public Body getBody() {
        return body;
    }

    @Override
    public void setDeathAnimation() {
        stateTimeAtkAndDeath += Gdx.graphics.getDeltaTime();
        body.setLinearVelocity(new Vector2(0, 0));
        setCurrentAnimation(deathAnimation.getKeyFrame(stateTimeAtkAndDeath));
        if (deathAnimation.isAnimationFinished(stateTimeAtkAndDeath))
            die();
    }

    @Override
    public void setAtkAnimation() {
        stateTimeAtkAndDeath += Gdx.graphics.getDeltaTime();
        if (!atkAnimation.isAnimationFinished(stateTimeAtkAndDeath)) {
            isStillAttacking = true;
            isAttacking = true;
            setCurrentAnimation(atkAnimation.getKeyFrame(stateTimeAtkAndDeath));
        } else {
            isStillAttacking = false;
            if (playerWeaponHitBox != null)
                deleteHitBox();
            updateAnimation();
        }
    }

    public void changeControls(int right, int left, int jump, int attack) { // custom controls
        controls.changeControls(right, left, jump, attack);
    }

    public void die() {
        if (playerWeaponHitBox != null)
            deleteHitBox();
        this.isDeleted = true;
        this.isDead = true;
        GameScreen.updateScreen = new GameOverScreen(game);
    }

    public void doorContact(Door door) {
        this.door = door;
    }

    public void atkHitBox() {
        if (!hasAtkHitBox) {
            playerWeaponHitBox = new AttackHitSensor(this, 35, 2, 5, 12, 2);
            hasAtkHitBox = true;
        }
    }

    public void deleteHitBox() {
        if (playerWeaponHitBox != null) {
            playerWeaponHitBox.destroy();
            playerWeaponHitBox = null;
        }
    }

    public AttackHitSensor getPlayerWeaponHitBox() {
        return playerWeaponHitBox;
    }

    /**
     * Equip player with the crown if he has picked it up, or beaten the game before
     */
    public void equipCrown() {
        game.stats.putBoolean("crown", true);
        game.stats.flush();

        super.setup("playerAnimations");
        if (isLookingLeft) {
            flipTexture();
        }
    }
}
