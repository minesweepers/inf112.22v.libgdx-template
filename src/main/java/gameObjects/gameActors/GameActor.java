package gameObjects.gameActors;


import gameObjects.GameObject;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import helper.HelperClass;

/**
 * For GameObjects that need a lot of animations, such as enemies and player
 * */
public abstract class GameActor extends GameObject {
    public boolean isDead = false;
    public boolean isLookingLeft = true;

    private int health;
    protected int contact = 0;

    private String folderName;

    private final float animationFrameDuration;
    private final int IDLE_FRAME_COLS;
    private final int IDLE_FRAME_ROWS;
    private final int RUN_FRAME_COLS;
    private final int RUN_FRAME_ROWS;
    private final int ATK_FRAME_COLS;
    private final int ATK_FRAME_ROWS;
    private final int DEATH_FRAME_COLS;
    private final int HIT_FRAME_COLS;

    private float stateTime;
    public float stateTimeAtkAndDeath;
    public float stateTimeGotHit;

    private TextureRegion currentAnimation;

    public Animation<TextureRegion> idleAnimation;
    public Animation<TextureRegion> runAnimation;
    public Animation<TextureRegion> fallAnimation;
    public Animation<TextureRegion> jumpAnimation;
    public Animation<TextureRegion> atkAnimation;
    public Animation<TextureRegion> deathAnimation;
    public Animation<TextureRegion> gotHitAnimation;

    public Texture texture;
    public Sprite sprite;
    public boolean gotHit;

    public GameActor(float animationFrameDuration,
                     int IDLE_FRAME_COLS, int IDLE_FRAME_ROWS, int RUN_FRAME_COLS,
                     int RUN_FRAME_ROWS, int ATK_FRAME_COLS, int ATK_FRAME_ROWS,
                     int DEATH_FRAME_COLS, int HIT_FRAME_COLS){
        this.animationFrameDuration = animationFrameDuration;
        this.IDLE_FRAME_COLS = IDLE_FRAME_COLS;
        this.IDLE_FRAME_ROWS = IDLE_FRAME_ROWS;
        this.RUN_FRAME_COLS = RUN_FRAME_COLS;
        this.RUN_FRAME_ROWS = RUN_FRAME_ROWS;
        this.ATK_FRAME_COLS = ATK_FRAME_COLS;
        this.ATK_FRAME_ROWS = ATK_FRAME_ROWS;
        this.DEATH_FRAME_COLS = DEATH_FRAME_COLS;
        this.HIT_FRAME_COLS = HIT_FRAME_COLS;
    }

    public void setup(String folderName){
        this.folderName = folderName;

        idleAnimation = idleAnimation();
        runAnimation = runAnimation();
        fallAnimation = fallAnimation();
        jumpAnimation = jumpAnimation();
        atkAnimation = atkAnimation();
        deathAnimation = deathAnimation();
        gotHitAnimation = gotHitAnimation();

        texture = new Texture(folderName+"/Fall.png");
        sprite = new Sprite(texture);
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getHealth(){
        return health;
    }

    public void damage(int damage){
        health -= damage;
    }

    public void increaseContact() {
        contact++;
    }

    public void decreaseContact() {
        contact--;
    }

    public boolean hasGroundContact() {
        return contact != 0;
    }

    public abstract void die();

    public Animation<TextureRegion> idleAnimation() {
        Texture idleTexture = new Texture(folderName+ "/Idle.png");
        return HelperClass.animationGen(idleTexture, IDLE_FRAME_COLS, IDLE_FRAME_ROWS, animationFrameDuration);
    }

    public Animation<TextureRegion> runAnimation() {
        Texture runTexture = new Texture(folderName+"/Run.png");
        return HelperClass.animationGen(runTexture, RUN_FRAME_COLS, RUN_FRAME_ROWS,animationFrameDuration);
    }

    public Animation<TextureRegion> fallAnimation() {
        Texture fallTexture = new Texture(folderName+"/Fall.png");
        return HelperClass.animationGen(fallTexture, 1, 1,animationFrameDuration);
    }

    public Animation<TextureRegion> jumpAnimation() {
        Texture jumpTexture = new Texture(folderName+"/Jump.png");
        return HelperClass.animationGen(jumpTexture, 1, 1,animationFrameDuration);
    }

    public Animation<TextureRegion> atkAnimation() {
        Texture atkTexture = new Texture(folderName+"/Attack.png");
        return HelperClass.animationGen(atkTexture, ATK_FRAME_COLS, ATK_FRAME_ROWS,0.15f);
    }

    public Animation<TextureRegion> deathAnimation() {
        Texture deathTexture = new Texture(folderName+"/Dead.png");
        return HelperClass.animationGen(deathTexture, DEATH_FRAME_COLS, 1,0.35f);
    }

    public Animation<TextureRegion> gotHitAnimation() {
        Texture hitAnimation = new Texture(folderName+"/Hit.png");
        return HelperClass.animationGen(hitAnimation, HIT_FRAME_COLS, 1,0.25f);
    }
    public void updateAnimation() {
        stateTime += Gdx.graphics.getDeltaTime();
        if (getBody().getLinearVelocity().y < 0) currentAnimation = fallAnimation.getKeyFrame(0,false);
        else if (getBody().getLinearVelocity().y > 0) currentAnimation = jumpAnimation.getKeyFrame(0,false);
        else if (getBody().getLinearVelocity().x == 0 && getBody().getLinearVelocity().y == 0) {
            currentAnimation = idleAnimation.getKeyFrame(stateTime,true);
        }
        else currentAnimation = runAnimation.getKeyFrame(stateTime, true);
    }

    public void setDeathAnimation(){
        stateTimeAtkAndDeath += Gdx.graphics.getDeltaTime();
        setCurrentAnimation(deathAnimation.getKeyFrame(stateTimeAtkAndDeath));
        if (deathAnimation.isAnimationFinished(stateTimeAtkAndDeath)) die();
    }

    public void setAtkAnimation(){
        stateTimeAtkAndDeath += Gdx.graphics.getDeltaTime();
        if (!atkAnimation.isAnimationFinished(stateTimeAtkAndDeath)){
            setCurrentAnimation(atkAnimation.getKeyFrame(stateTimeAtkAndDeath));
        }
        else {
            updateAnimation();
        }
    }
    public void setGotHitAnimation(){
        stateTimeGotHit += Gdx.graphics.getDeltaTime();
        if (!gotHitAnimation.isAnimationFinished(stateTimeGotHit)){
            setCurrentAnimation(gotHitAnimation.getKeyFrame(stateTimeGotHit));
        }
        else {
            updateAnimation();
            gotHit = false;
        }
    }


    public TextureRegion getCurrentAnimation(){
        return currentAnimation;
    }

    public void setCurrentAnimation(TextureRegion animation){
        currentAnimation = animation;
    }

    public void flipTexture(){
        flipTexture(idleAnimation);
        flipTexture(runAnimation);
        flipTexture(fallAnimation);
        flipTexture(jumpAnimation);
        flipTexture(atkAnimation);
        flipTexture(deathAnimation);
        flipTexture(gotHitAnimation);
    }
}
