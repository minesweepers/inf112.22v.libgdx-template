package mapObjects;

import com.badlogic.gdx.math.Vector2;
import gameObjects.IGameObject;
import helper.Constants;

public abstract class MapObject implements IGameObject {

	public Vector2 getPosition(){
		return getBody().getPosition();
	}

    public float[] scaleVertices(float[] vertices) {
    	for (int i = 0; i < vertices.length; i++) {
    		vertices[i] *= Constants.SCALE;
    	}
    	
    	return vertices;
    }
}
