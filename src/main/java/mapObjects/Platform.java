package mapObjects;

import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.physics.box2d.*;

import helper.Constants;

public class Platform extends MapObject {
    private final Body body;

    public Platform(World world, PolygonMapObject tile) {
        BodyDef tileBodyDef = new BodyDef();

        //Set world position
        tileBodyDef.position.set(tile.getPolygon().getX() * Constants.SCALE, tile.getPolygon().getY() * Constants.SCALE);
        tileBodyDef.type = BodyDef.BodyType.StaticBody;

        //Create a body from the definition and add it to the world
        body = world.createBody(tileBodyDef);
        body.setUserData(this);

        //setAsBox takes half-width and half-height as arguments
        FixtureDef fixtureDef = new FixtureDef();

        PolygonShape tileShape = new PolygonShape();
        float[] vertices = scaleVertices(tile.getPolygon().getVertices());
        
        tileShape.set(vertices);
        fixtureDef.shape = tileShape;

        body.createFixture(fixtureDef);
        tileShape.dispose();
    }

    @Override
    public Body getBody() {
        return body;
    }
}
