package mapObjects;

import com.badlogic.gdx.math.Vector2;
import helper.Constants;

import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.physics.box2d.*;
import helper.HelperClass;
import helper.LevelMap;
import inf112.minesweepers.LevelBuilder;
import inf112.minesweepers.LevelSwitcher;

/**
 * A door can either lead to another door or load another map.
 *
 * By default, the door will lead to the next level determined by DoorMap.java.
 * So if a door has no name -> checks HashMap for next level.
 *
 * If two doors are given two identical numbers as names in the map editor they will be linked.
 * 1 -> 1 and 1 -> 1
 *
 * Doors can be one way, so if a door with value 1 can lead to a door with value 2.
 * 1 -> 2, but 2 -/> 1
 *
 * A door can have a custom property set to true: "isLocked", so it requires a player to collect a crown before opening.
 * */
public class Door extends MapObject {
    private final String name;
    private Integer number = null;
    private boolean isLocked = false;

    private final Body body;
    private LevelSwitcher levelSwitcher;

    public Door(World world, PolygonMapObject tile) {
        name = tile.getName();
        Boolean isLocked = (Boolean) tile.getProperties().get("isLocked");
        if (isLocked != null){
            this.isLocked = isLocked;
        }
        if (HelperClass.isInteger(name)){
            number = Integer.parseInt(name);
        }

        BodyDef tileBodyDef = new BodyDef();

        //Set world position
        tileBodyDef.position.set(tile.getPolygon().getX() * Constants.SCALE,tile.getPolygon().getY() * Constants.SCALE);
        tileBodyDef.type = BodyDef.BodyType.StaticBody;

        //Create a body from the definition and add it to the world
        body = world.createBody(tileBodyDef);
        body.setUserData(this);

        //setAsBox takes half-width and half-height as arguments
        FixtureDef fixtureDef = new FixtureDef();

        PolygonShape tileShape = new PolygonShape();
        float[] vertices = scaleVertices(tile.getPolygon().getVertices());
        
        tileShape.set(vertices);
        fixtureDef.shape = tileShape;
        fixtureDef.isSensor = true;

        body.createFixture(fixtureDef);
        tileShape.dispose();
    }

    @Override
    public Body getBody() {
        return body;
    }

    public void isLocked(boolean b) {
        isLocked = b;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void changeLevel() {
        levelSwitcher.nextLevel = LevelMap.levelMap.get(levelSwitcher.currentLevel);
    }

    public Integer getNumber() {
        return number;
    }

    public void setSwitcher(LevelSwitcher levelSwitcher) {
        this.levelSwitcher = levelSwitcher;
    }

    public Vector2 teleport() {
        for (Body tileBody : LevelBuilder.tileBodies){
            Object obj = tileBody.getUserData();
            if (obj instanceof Door door){

                //Ignore level switching doors and same door
                if (door.getNumber() == null || this == door){
                    continue;
                }

                //Linked doors
                if (door.getNumber().equals(this.number)){
                    Vector2 pos = door.getBody().getPosition();
                    return new Vector2(pos.x + 23*Constants.SCALE, pos.y - 40*Constants.SCALE);
                }
                //One way door
                else if (door.getNumber() == this.number+1){
                    Vector2 pos = door.getBody().getPosition();
                    return new Vector2(pos.x + 23*Constants.SCALE, pos.y - 40*Constants.SCALE);
                }
            }
        }
        return null;
    }
}
