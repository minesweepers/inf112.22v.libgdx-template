package helper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

import java.util.Random;

public class AudioHandler {
	private static final Sound pickupSound = Gdx.audio.newSound(Gdx.files.internal("sound/pickup.wav"));
	private static final Sound attackSound = Gdx.audio.newSound(Gdx.files.internal("sound/attack.wav"));
	private static final Sound explosionSound = Gdx.audio.newSound(Gdx.files.internal("sound/explode.wav"));
	private static final Sound jumpSound = Gdx.audio.newSound(Gdx.files.internal("sound/jump.wav"));

	private static final Sound hurtSound1 = Gdx.audio.newSound(Gdx.files.internal("sound/hurt.wav"));
	private static final Sound hurtSound2 = Gdx.audio.newSound(Gdx.files.internal("sound/hurt2.wav"));

	private static final Sound doorOpenSound = Gdx.audio.newSound(Gdx.files.internal("sound/open.wav"));
	private static final Sound doorLockedSound = Gdx.audio.newSound(Gdx.files.internal("sound/locked.wav"));

	public static void playPickup() {
		pickupSound.play();
	}

	public static void playAttack() {
		attackSound.play();
	}

	public static void playExplosion() {
		explosionSound.play();
	}

	public static void playJump() {
		jumpSound.play();
	}

	public static void playHurt() {
		Random rnd = new Random();
		int i = rnd.nextInt(2);

		if (i == 1){
			hurtSound1.play();
		}
		else{
			hurtSound2.play();
		}
	}

	public static void playOpenDoor() {
		doorOpenSound.play();
	}

	public static void playLockedDoor() {
		doorLockedSound.play();
	}
}
