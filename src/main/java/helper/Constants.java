package helper;

public class Constants {
	public static final float SCALE = 0.01f;
	public static final float TIME_STEP = 1f / 60f;
	public static final float PIXELS_PER_UNIT = 1f / SCALE;
	public static final float SCREEN_EDGE = 1024 * SCALE;
}
