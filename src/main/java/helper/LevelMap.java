package helper;

import java.util.HashMap;

public class LevelMap {
    public final static HashMap<String,String> levelMap = new HashMap<>();

    /**
     * A map containing the level names of where doors lead to.
     *
     * Level A -> Level B
     * */
    public static void createDoorMap(){
        levelMap.clear();
        levelMap.put("cabin","outdoor");
        levelMap.put("outdoor","outdoor2");
        levelMap.put("outdoor2","dungeon");
        levelMap.put("dungeon", "dungeon2");
        levelMap.put("dungeon2", "dungeon3");
        levelMap.put("dungeon3", "bossRoom");
        levelMap.put("bossRoom", "outdoor");
        levelMap.put("test1", "test2");
    }
}
