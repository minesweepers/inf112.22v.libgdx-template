package helper;

import gameObjects.gameActors.enemies.BossPig;
import gameObjects.gameActors.enemies.Enemy;
import gameObjects.items.Bomb;
import gameObjects.items.Cannon;
import gameObjects.items.CannonBall;
import gameObjects.items.Item;
import com.badlogic.gdx.Gdx;
import gameObjects.GameObject;
import gameObjects.gameActors.players.Player;

import inf112.minesweepers.Pigsweepers;

public record Renderer(Pigsweepers game) {

    //Move player and update sprite position
    public void superRenderer(GameObject object) {
        game.batch.setProjectionMatrix(game.camera.combined);
        game.batch.begin();

        if (object instanceof Player p) {
            playerRender(p);
        } else if (object instanceof Item item) {
            itemRender(item);
        } else if (object instanceof Enemy enemy) {
            enemyRender(enemy);
        }
        game.batch.end();
    }

    private void enemyRender(Enemy enemy) {
        enemy.move();
        if (enemy.gotHit) {
            enemy.setGotHitAnimation();
        } else if (!enemy.isDead) {
            if (enemy.isAttacking) {
                enemy.setAtkAnimation();
                if (enemy.stateTimeAtkAndDeath > 0.25f) //adds a 0.25 delay before the pig does damage if ur inside its attackrange
                    enemy.atkHitBox();
            } else {
                if (enemy.hasAtkHitBox()) enemy.setHasAtkHitBox(false);
                if (enemy.getEnemyAttackHitBox() != null) {
                    enemy.deleteHitBox();
                }
                enemy.updateAnimation();
                enemy.stateTimeAtkAndDeath = 0;
            }
        } else {
            enemy.setDeathAnimation();
        }
        float width = getWidth(enemy);
        float height = getHeight(enemy);

        if (enemy instanceof BossPig){
            float pigHitBoxFix = 30*Constants.SCALE;
            if (enemy.getMovementSpeed() > 0) pigHitBoxFix = 25 * Constants.SCALE;
            game.batch.draw(enemy.getCurrentAnimation(), enemy.getPosition().x - width/2 - pigHitBoxFix,enemy.getPosition().y - height/2 - (10 * Constants.SCALE), width*2.5f, height*2.5f);
        }
        //Enemies that have the same size as EnemyPig and FlyingPig
        else{
            float pigHitBoxFix = 12*Constants.SCALE;
            if (enemy.getMovementSpeed() > 0) pigHitBoxFix = 2 * Constants.SCALE;
            game.batch.draw(enemy.getCurrentAnimation(), enemy.getPosition().x - width/2 - pigHitBoxFix,enemy.getPosition().y - height/2, width*1.5f, height*1.5f);
        }
    }

    private void itemRender(Item item) {
        item.updateAnimation();

        float width = getWidth(item);
        float height = getHeight(item);
        if (item instanceof Bomb b) {
            if (!b.isExploded()){
                game.batch.draw(item.getCurrentAnimation(), item.getPosition().x - width/2 - (27*Constants.SCALE),item.getPosition().y - height/2 - (18*Constants.SCALE), width*2, height*2);
            }
        }
        else if (item instanceof Cannon){
            game.batch.draw(item.getCurrentAnimation(), item.getPosition().x - width/2 - (25*Constants.SCALE),item.getPosition().y - height/2 - (8*Constants.SCALE), width*2, height*2);
        }
        else if (item instanceof CannonBall){
            game.batch.draw(item.getCurrentAnimation(), item.getPosition().x - width/2 - (38*Constants.SCALE),item.getPosition().y - height/2 - (3*Constants.SCALE), width*2, height*2);
        }
        else {
            game.batch.draw(item.getCurrentAnimation(), item.getPosition().x - width/2 - (14*Constants.SCALE),item.getPosition().y - height/2 - (8*Constants.SCALE), width*2, height*2);
        }
    }

    private void playerRender(Player p) {
        p.movePlayer(Gdx.input);

        if (p.gotHit) p.setGotHitAnimation();
        else if (!p.isDead) {
            if (p.isAttacking || p.isStillAttacking) {
                p.setAtkAnimation();
                p.atkHitBox();
            } else {
                if (p.hasAtkHitBox) {
                    p.hasAtkHitBox = false;
                }
                if (p.getPlayerWeaponHitBox() != null) {
                    p.deleteHitBox();
                }
                p.updateAnimation();
                p.stateTimeAtkAndDeath = 0;
            }
        } else {
            p.setDeathAnimation();
        }
        float hitboxFix = -10f * Constants.SCALE;
        if (p.isLookingLeft) hitboxFix = 10 * Constants.SCALE;

        float width = getWidth(p);
        float height = getHeight(p);

        game.batch.draw(p.getCurrentAnimation(), p.getPosition().x - (width / 2) - hitboxFix,p.getPosition().y - height/2, width, height);
    }

    private float getWidth(GameObject gameObject) {
        return gameObject.getCurrentAnimation().getRegionWidth() * Constants.SCALE;
    }

    private float getHeight(GameObject gameObject) {
        return gameObject.getCurrentAnimation().getRegionHeight() * Constants.SCALE;
    }
}
