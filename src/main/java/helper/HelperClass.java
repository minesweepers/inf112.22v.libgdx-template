package helper;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
public class HelperClass {

    public static Animation<TextureRegion> animationGen(Texture texture, int FRAME_COLS, int FRAME_ROWS,float framedurr) {
        TextureRegion[][] run = TextureRegion.split(texture,
                texture.getWidth() / FRAME_COLS,
                texture.getHeight() / FRAME_ROWS);
        TextureRegion[] walkFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];
        int index = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
            for (int j = 0; j < FRAME_COLS; j++) {
                walkFrames[index++] = run[i][j];
            }
        }

        return new Animation<>(framedurr, walkFrames);
    }

    public static boolean isInteger(String str){
        return str != null && str.matches("^[0-9]+");
    }
}