package gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import inf112.minesweepers.Pigsweepers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameOverScreen implements Screen {
    private final Pigsweepers game;
    private final String proTip;
    private final Texture gameOverScreen;

    private Stage stage;
    private BitmapFont gameOverFont;

    public GameOverScreen(Pigsweepers game) {
        this.game = game;
        gameOverScreen = new Texture("gui/GameOverScreen.png");

        //Find a very helpful proTip
        ArrayList<String> stringArray = new ArrayList<>(
                List.of("If your HP is less than 0, this screen will appear!",
                        "Don't die.",
                        "...",
                        "Just try again",
                        "You got this!",
                        "Did you win?",
                        "Use SPACE to go trough doors!",
                        "Ignite bombs by hitting them with your hammer!",
                        "You have won "+ game.settings.getInteger("totalWins") +" times.",
                        "git gud",
                        "Hit the enemies harder next time!",
                        "Do it, [PLAYER NAME HERE], make [PLAYER PARENT HERE] proud!",
                        "''It is more important to outhink your enemy, than to outfight him'' -Sun Tzu",
                        "According to all known laws of aviation, there is no way that a bee should be able to fly. Its wings are too small to get its fat little body off the ground. The bee, of course, flies anyway because bees don't care what humans think is impossible."
                )
        );

        Random rnd = new Random();
        int index = rnd.nextInt(stringArray.size());
        proTip = stringArray.get(index);

        createFont();
        createGameOverScreen();
    }

    private void createFont() {
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 50;
        parameter.borderWidth = 2;
        gameOverFont = game.generator.generateFont(parameter);
    }

    private void createGameOverScreen() {
        float width = Gdx.graphics.getWidth();
        float height = Gdx.graphics.getHeight();

        stage = new Stage(new FitViewport(width,height), game.batch);
        stage.addActor(createTable(height));
    }

    private Table createTable(float height) {
        TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();
        buttonStyle.font = gameOverFont;

        Label gameOverLabel = new Label("Game Over", new Label.LabelStyle(gameOverFont, Color.WHITE));
        Label proTipLabel = new Label("Pro Tip: "+proTip, new Label.LabelStyle(game.font, Color.WHITE));


        TextButton respawnButton = new TextButton("Respawn", buttonStyle);
        respawnButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new GameScreen(game));
            }
        });

        TextButton mainMenuButton = new TextButton("Main Menu", buttonStyle);
        mainMenuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MainMenuScreen(game));
            }
        });
        TextButton rageQuitButton = new TextButton("Quit", buttonStyle);
        rageQuitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });

        Table table = new Table();
        //table.setDebug(game.debug); // turn on all debug lines
        table.center();
        table.setFillParent(true);

        table.add(gameOverLabel).expandX().padTop(height/10);
        table.row().expandY();
        table.add(respawnButton).expandX().padTop(height/10);
        table.row().expandY();
        table.add(mainMenuButton).expandX().padTop(height/10);
        table.row().expandY();
        table.add(rageQuitButton).expandX();
        table.row().expandY();
        table.add(proTipLabel).expandX().padBottom(height/10);

        return table;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
    	game.batch.setProjectionMatrix(game.camera.combined);
		game.batch.begin();
		game.batch.draw(gameOverScreen, 0, 0, game.camera.viewportWidth, game.camera.viewportHeight);
		game.batch.end();

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            game.setScreen(new GameScreen(game));
        }

        Gdx.input.setInputProcessor(stage);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        createGameOverScreen();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

}
