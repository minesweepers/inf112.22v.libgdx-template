package gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import inf112.minesweepers.Pigsweepers;

public class MainMenuScreen implements Screen {
	private final Pigsweepers game;
	private final Texture titleScreen;
	private Stage menuStage;
	private BitmapFont menuFont;

	public MainMenuScreen(Pigsweepers game) {
		this.game = game;
		titleScreen = new Texture("gui/TitleScreenNoText.png");

		createFont();
		createMainMenu();
	}

	private void createFont() {
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = 55;
		parameter.borderWidth = 2;
		menuFont = game.generator.generateFont(parameter);
	}

	private void createMainMenu() {
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		menuStage = new Stage(new FitViewport(width, height), game.batch);
		menuStage.addActor(createTable(height));
	}

	private Table createTable(int height) {
		TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();
		buttonStyle.font = menuFont;

		Table table = new Table();
		//table.setDebug(true); // turn on all debug lines
		table.center();
		table.setFillParent(true);

		TextButton localPlayButton = new TextButton("Local Play", buttonStyle);
		localPlayButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (game.debug){
					game.setScreen(new TestSelectScreen(game));
				}
				else{
					if (game.save.getString("currentLevel").startsWith("test")){
						game.settings.clear();
						game.settings.flush();

						game.save.clear();
						game.save.flush();

						game.stats.clear();
						game.stats.flush();
					}
					game.setScreen(new LocalPlayScreen(game));
				}
			}
		});

		//TODO: Implement start online
		TextButton onlinePlayButton = new TextButton("Online PLay", buttonStyle);
		onlinePlayButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("ONLINE PLAY NOT IMPLEMENTED YET");
			}
		});

		TextButton optionsButton = new TextButton("Options", buttonStyle);
		optionsButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new SettingsScreen(game));
			}
		});
		
		TextButton creditsButton = new TextButton("Credits", buttonStyle);
		creditsButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new CreditsScreen(game));
			}
		});

		TextButton quitButton = new TextButton("Quit", buttonStyle);
		quitButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
			}
		});

		table.center();
		table.add(localPlayButton).padTop(height/4f);
		table.row();
		table.add(onlinePlayButton);
		table.row();
		table.add(optionsButton);
		table.row();
		table.add(creditsButton);
		table.row();
		table.add(quitButton).padBottom(height/8f);

		return table;
	}

	@Override
	public void show() {
	}

	@Override
	public void render(float delta) {
		game.batch.setProjectionMatrix(game.camera.combined);
		game.batch.begin();
		game.batch.draw(titleScreen, 0, 0, game.camera.viewportWidth, game.camera.viewportHeight);
		game.batch.end();

		Gdx.input.setInputProcessor(menuStage);
		menuStage.draw();
	}

	@Override
	public void resize(int width, int height) {
		createMainMenu();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
	}
}