package gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import inf112.minesweepers.Pigsweepers;

public class LocalPlayScreen implements Screen {
    private final Pigsweepers game;
    private final Texture background;

    private Stage stage;
    private BitmapFont localPlayFont;

    public LocalPlayScreen(Pigsweepers game){
        this.game = game;
        background = new Texture("gui/TitleScreenNoText.png");

        createFont();
        createLocalScreen();
    }


    private void createFont() {
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 60;
        parameter.borderWidth = 2;
        localPlayFont = game.generator.generateFont(parameter);
    }


    private void createLocalScreen() {
        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        stage = new Stage(new FitViewport(width, height), game.batch);
        stage.addActor(createTable(height));
    }

    private Table createTable(int height) {
        TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();
        buttonStyle.font = localPlayFont;

        Table table = new Table();
        //table.setDebug(true); // turn on all debug lines
        table.center();
        table.setFillParent(true);

        TextButton soloButton = new TextButton("1 Player", buttonStyle);
        soloButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.mpOn = false;
                game.setScreen(new GameScreen(game));
            }
        });

        TextButton coopButton = new TextButton("2 Player", buttonStyle);
        coopButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.mpOn = true;
                game.setScreen(new GameScreen(game));
            }
        });


        table.center();
        table.add(soloButton).padTop(height/5f);
        table.row().padTop(height/8f);
        table.add(coopButton).padBottom(height/8f);

        return table;
    }


    @Override
    public void show() {

    }

    @Override
    public void render(float v) {
        game.batch.setProjectionMatrix(game.camera.combined);
        game.batch.begin();
        game.batch.draw(background, 0, 0, game.camera.viewportWidth, game.camera.viewportHeight);
        game.batch.end();

        Gdx.input.setInputProcessor(stage);
        stage.draw();
    }

    @Override
    public void resize(int i, int i1) {
        createLocalScreen();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
