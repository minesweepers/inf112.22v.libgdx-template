package gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import inf112.minesweepers.Pigsweepers;

public class TestSelectScreen implements Screen {
    private final Pigsweepers game;
    private final Texture background;

    private Stage stage;
    private BitmapFont localPlayFont;

    public TestSelectScreen(Pigsweepers game){
        this.game = game;
        background = new Texture("gui/TitleScreenNoText.png");

        createFont();
        createTestScreen();
    }


    private void createFont() {
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 40;
        parameter.borderWidth = 2;
        localPlayFont = game.generator.generateFont(parameter);
    }


    private void createTestScreen() {
        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        stage = new Stage(new FitViewport(width, height), game.batch);
        stage.addActor(createTable(height));
    }

    private Table createTable(int height) {
        TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();
        buttonStyle.font = localPlayFont;

        Table table = new Table();
        //table.setDebug(true); // turn on all debug lines
        table.center();
        table.setFillParent(true);

        Label.LabelStyle labelStyle = new Label.LabelStyle(game.font, Color.RED);
        Label warningLabel = new Label("WARNING: Playing with debug mode enabled will reset your save data.\nBackup important data before proceeding!", labelStyle);

        TextButton soloButton = new TextButton("Test1: Items, Doors", buttonStyle);
        soloButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //game.prefs = Gdx.app.getPreferences("TestPigsweepers");
                game.save.clear();
                game.save.putString("currentLevel", "test1");
                game.save.putInteger("playerHp", 4);
                game.save.flush();
                game.mpOn = true;
                game.setScreen(new GameScreen(game));
            }
        });

        TextButton coopButton = new TextButton("Test2: Enemy, Bombs and Boxes", buttonStyle);
        coopButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.save.clear();
                game.save.putString("currentLevel", "test2");
                game.save.flush();
                game.mpOn = true;
                game.setScreen(new GameScreen(game));
            }
        });

        TextButton mainMenuButton = new TextButton("Main Menu", buttonStyle);
        mainMenuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MainMenuScreen(game));
            }
        });

        table.center();
        table.add(warningLabel).padTop(height/6f);
        table.row();
        table.add(soloButton);
        table.row();
        table.add(coopButton);
        table.row().padTop(height/8f);
        table.add(mainMenuButton).padBottom(height/8f);

        return table;
    }


    @Override
    public void show() {

    }

    @Override
    public void render(float v) {
        game.batch.setProjectionMatrix(game.camera.combined);
        game.batch.begin();
        game.batch.draw(background, 0, 0, game.camera.viewportWidth, game.camera.viewportHeight);
        game.batch.end();

        Gdx.input.setInputProcessor(stage);
        stage.draw();
    }

    @Override
    public void resize(int i, int i1) {
        createTestScreen();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
