package gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;

import inf112.minesweepers.Pigsweepers;

public class CreditsScreen implements Screen {
	private final Pigsweepers game;
	private final Texture creditsScreen;

	public CreditsScreen(Pigsweepers game) {
		this.game = game;
		creditsScreen = new Texture("gui/CreditsScreen.png");
	}


	@Override
	public void show() {
	}

	@Override
	public void render(float delta) {
		game.batch.setProjectionMatrix(game.camera.combined);
		game.batch.begin();
		game.batch.draw(creditsScreen, 0, 0, game.camera.viewportWidth, game.camera.viewportHeight);
		game.batch.end();

		if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE) || Gdx.input.justTouched()) {
			game.setScreen(new MainMenuScreen(game));
		}
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
	}
}