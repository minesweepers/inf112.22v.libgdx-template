package gameScreens;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import gameObjects.GameObject;
import gameObjects.gameActors.GameActor;
import gameObjects.gameActors.players.Player;
import helper.Constants;
import helper.Renderer;
import inf112.minesweepers.ContactHandler;
import inf112.minesweepers.Hud;
import inf112.minesweepers.LevelSwitcher;
import inf112.minesweepers.Pigsweepers;

public class GameScreen implements Screen {
    private final float GRAVITY = -10;
    private float accumulator = 0.0f;

    private final Pigsweepers game;
    private final Renderer gameRenderer;
    private final World world;
    private final LevelSwitcher levelSwitcher;

    public static Hud hud;
    public final static ArrayList<GameObject> addToRender = new ArrayList<>();
    public final static ArrayList<GameObject> gameObjectsToRender = new ArrayList<>();
    public final static Array<Body> bodiesToDestroy = new Array<>();
    public static Screen updateScreen;

    public GameScreen(Pigsweepers game) {
        this.game = game;

        // Create a new physics world with a contact handler
        world = new World(new Vector2(0, GRAVITY), true);
        addToRender.clear();
        gameObjectsToRender.clear();
        bodiesToDestroy.clear();
        updateScreen = null;

        // Create player objects and add to thingsToRender
        createPlayers();

        hud = new Hud(game);
        gameRenderer = new Renderer(game);

        String level = "cabin";
        if (game.save.contains("currentLevel")) {
            level = game.save.getString("currentLevel");
        } else {
            game.save.putString("currentLevel", "cabin");
            game.save.flush();
        }
        levelSwitcher = new LevelSwitcher(game, world, level);

        world.setContactListener(new ContactHandler(levelSwitcher));
    }

    private void doPhysicsStep(float deltaTime) {
        float frameTime = Math.min(deltaTime, 0.25f);
        accumulator += frameTime;

        while (accumulator >= Constants.TIME_STEP) {
            world.step(Constants.TIME_STEP, 6, 2);
            accumulator -= Constants.TIME_STEP;
        }
    }

    private void renderThingsToRender() {
        // For destroying bodies such as sensors safely
        Iterator<Body> bodyIterator = bodiesToDestroy.iterator();
        while (bodyIterator.hasNext()) {
            Body body = bodyIterator.next();

            if (!world.isLocked()) {
                world.destroyBody(body);
                bodyIterator.remove();
            }
        }

        // For adding things to render
        Iterator<GameObject> addToRenderIterator = addToRender.iterator();
        while (addToRenderIterator.hasNext()) {
            GameObject gameObject = addToRenderIterator.next();
            gameObjectsToRender.add(gameObject);
            addToRenderIterator.remove();
        }

        // For destroying gameObjects safely
        Iterator<GameObject> gameObjectIterator = gameObjectsToRender.iterator();
        while (gameObjectIterator.hasNext()) {
            GameObject gameObject = gameObjectIterator.next(); // must be called before remove
            gameRenderer.superRenderer(gameObject);

            if (destroyDeleted(gameObject)) {
                gameObjectIterator.remove();
            }
        }
    }

    private boolean destroyDeleted(GameObject obj) {
        if (obj.isDeleted) {
            if (obj instanceof GameActor gameActor) { // For destroying pig sensors
                if (!gameActor.isDead) {
                    gameActor.die();
                }
            }
            if (!world.isLocked()) {
                world.destroyBody(obj.getBody());
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    private void createPlayers() {
        // Add player objects
        Player player1 = new Player(new Vector2(0, 0), world, true, game);
        gameObjectsToRender.add(player1);
        if (game.mpOn) {
            Player player2 = new Player(new Vector2(0, 0), world, false, game);
            player2.changeControls(Input.Keys.D, Input.Keys.A, Input.Keys.W, Input.Keys.S);
            gameObjectsToRender.add(player2);
        }
    }

    @Override
    public void render(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            game.isPaused = !game.isPaused;
        }

        // Check if there is a new level to change to.
        levelSwitcher.checkLevel();

        if (updateScreen != null) {
            game.setScreen(updateScreen);
            return;
        }

        // Render map
        levelSwitcher.getRenderer().render();

        // Render gameObjects and remove deleted ones
        renderThingsToRender();

        // Draw the hud
        hud.stage.draw();
        hud.renderHp();

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE) && hud.note) {
            hud.note = false;
        }

        // Step forward world simulation
        Gdx.input.setInputProcessor(game.pausedStage);
        if (!game.isPaused) {
            doPhysicsStep(Gdx.graphics.getDeltaTime());
        } else {
            game.pausedStage.draw();
        }
        if (game.debug) {
            // System.out.println(Gdx.graphics.getFramesPerSecond());
            new Box2DDebugRenderer().render(world, game.camera.combined); // For debugging, shows hit boxes
        }
    }

    @Override
    public void show() {
    }

    @Override
    public void resize(int width, int height) {
        hud.setHud(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    }
}
