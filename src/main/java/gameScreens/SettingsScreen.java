package gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import helper.HelperClass;
import inf112.minesweepers.Pigsweepers;

import java.util.ArrayList;
import java.util.List;

public class SettingsScreen implements Screen {
    private final Pigsweepers game;
    private Stage settingsStage;

    //Current settings:
    private boolean vSyncOn;
    private FpsMode fpsMode;
    private WindowMode windowMode;
    private boolean clearSaveData;
    private boolean clearAllData;
    private boolean debug;

    private final ArrayList<FpsMode> fpsOptions;
    private final ArrayList<WindowMode> windowOptions;

    private TextField inputWidth;
    private TextField inputHeight;

    private enum FpsMode {
        F30(30),
        F60(60),
        F144(144),
        F165(165),
        F240(240),
        UNLOCKED(0);

        public final int fps;

        FpsMode(int fps) {
            this.fps = fps;
        }
    }

    private enum WindowMode {
        FULLSCREEN,
        BORDERLESS_WINDOWED,
        WINDOWED,
        CUSTOM
    }

    public SettingsScreen(Pigsweepers game) {
        this.game = game;
        clearSaveData = false;
        clearAllData = false;
        debug = game.debug;

        fpsOptions = new ArrayList<>(List.of(FpsMode.values()));
        windowOptions = new ArrayList<>(List.of(WindowMode.values()));

        //Find saved settings
        getSettings();

        createPauseScreen();
    }

    /**
     * Apply the saved video-settings on game startup
     */
    public void startUp() {
        getSettings();
        apply();
    }

    /**
     * Updates the field variables to the saved values, if any
     */
    private void getSettings() {
        debug = game.settings.getBoolean("debug");
        vSyncOn = game.settings.getBoolean("vSync");

        fpsMode = FpsMode.F240;
        if (game.settings.contains("fpsMode")) {
            int savedFps = game.settings.getInteger("fpsMode");
            for (FpsMode fpsM : fpsOptions) {
                if (fpsM.fps == savedFps) {
                    fpsMode = fpsM;
                }
            }
        }

        windowMode = WindowMode.WINDOWED;
        if (game.settings.contains("windowMode")) {
            String savedMode = game.settings.getString("windowMode");
            for (WindowMode w : windowOptions) {
                if (savedMode.equals(w.toString())) {
                    windowMode = w;
                }
            }
        }
    }

    /**
     * Take the field variables into effect.
     */
    private void apply() {
        if (inputWidth != null && inputHeight != null){
            if (HelperClass.isInteger(inputWidth.getText()) && HelperClass.isInteger(inputHeight.getText())){
                int width = Integer.parseInt(inputWidth.getText());
                int height = Integer.parseInt(inputHeight.getText());
                if (width >= 100 && width <= 7680 && height >= 100 && height <= 4360){
                    game.settings.putInteger("customWidth", width);
                    game.settings.putInteger("customHeight", height);
                }
            }
        }

        switch (windowMode) {
            case WINDOWED:
                Gdx.graphics.setWindowedMode(1024, 512);
                Gdx.graphics.setResizable(true);
                break;
            case FULLSCREEN:
                Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
                break;
            case BORDERLESS_WINDOWED:
                Graphics.DisplayMode mode = Gdx.graphics.getDisplayMode();
                Gdx.graphics.setWindowedMode(mode.width, mode.height-10);
                break;
            case CUSTOM:
                if (game.settings.contains("customWidth") && game.settings.contains("customHeight")){
                    int width = game.settings.getInteger("customWidth");
                    int height = game.settings.getInteger("customHeight");
                    Gdx.graphics.setWindowedMode(width,height);
                }
                break;
        }
        Gdx.graphics.setForegroundFPS(fpsMode.fps);
        Gdx.graphics.setVSync(vSyncOn);

        game.debug = debug;

        game.settings.putBoolean("vSync", vSyncOn);
        game.settings.putInteger("fpsMode", fpsMode.fps);
        game.settings.putString("windowMode", windowMode.toString());
        game.settings.putBoolean("debug", debug);
        game.settings.flush();

        //Clear all save data including setting preset
        if (clearAllData){
            game.settings.clear();
            game.settings.flush();

            game.stats.clear();
            game.stats.flush();

            game.save.clear();
            game.save.flush();

            clearAllData = false;
            clearSaveData = false;
            startUp();
        }
        else if (clearSaveData) {
            game.save.clear();
            game.save.flush();
        }
    }

    private void createPauseScreen() {
        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        settingsStage = new Stage(new FitViewport(width, height), game.batch);
        settingsStage.addActor(createTable());
    }

    private Table createTable() {
        Label.LabelStyle labelStyle = new Label.LabelStyle(game.font, Color.WHITE);

        TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();
        buttonStyle.font = game.font;

        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle();
        textFieldStyle.font = game.font;
        textFieldStyle.fontColor = Color.WHITE;

        Table table = new Table();
        table.center();
        table.setFillParent(true);

        Label settingsLabel = new Label("Game Settings", labelStyle);

        inputWidth = new TextField("Width:", textFieldStyle);
        inputHeight = new TextField("Height:", textFieldStyle);

        TextField.TextFieldFilter filter = new TextField.TextFieldFilter.DigitsOnlyFilter();

        inputWidth.setTextFieldFilter(filter);
        inputHeight.setTextFieldFilter(filter);

        inputWidth.setVisible(windowMode.equals(WindowMode.CUSTOM));
        inputHeight.setVisible(windowMode.equals(WindowMode.CUSTOM));

        inputWidth.addCaptureListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                inputWidth.setText("");
                super.clicked(event, x, y);
            }
        });
        inputHeight.addCaptureListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                inputHeight.setText("");
                super.clicked(event, x, y);
            }
        });

        TextButton displayModeButton = new TextButton("Display Mode: " + windowMode, buttonStyle);
        displayModeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                int i = windowOptions.indexOf(windowMode) + 1;
                if (i >= windowOptions.size()) {
                    i = 0;
                }
                windowMode = windowOptions.get(i);
                if (windowMode.equals(WindowMode.CUSTOM)){
                    inputWidth.setVisible(true);
                    inputHeight.setVisible(true);
                }
                else{
                    inputWidth.setTextFieldFilter(null);
                    inputHeight.setTextFieldFilter(null);

                    inputWidth.setText("Width:");
                    inputHeight.setText("Height:");

                    inputWidth.setTextFieldFilter(filter);
                    inputHeight.setTextFieldFilter(filter);

                    inputWidth.setVisible(false);
                    inputHeight.setVisible(false);
                }
                displayModeButton.setText("Display Mode: " + windowMode);
            }
        });

        TextButton vSyncModeButton = new TextButton("VSync:" + vSyncOn, buttonStyle);
        vSyncModeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                vSyncOn = !vSyncOn;
                vSyncModeButton.setText("VSync: " + vSyncOn);
            }
        });

        TextButton fpsModeButton = new TextButton("FPS: " + fpsMode.fps, buttonStyle);
        fpsModeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                int i = fpsOptions.indexOf(fpsMode) + 1;
                if (i >= fpsOptions.size()) {
                    i = 0;
                }
                fpsMode = fpsOptions.get(i);
                fpsModeButton.setText("FPS: " + fpsMode.fps);
            }
        });

        TextButton debugButton = new TextButton("Debug: "+debug, buttonStyle);
        debugButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                debug = !debug;
                debugButton.setText("Debug: " + debug);
            }
        });

        TextButton clearButton = new TextButton("CLEAR SAVE DATA: " + clearSaveData, buttonStyle);
        clearButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                clearSaveData = !clearSaveData;
                clearButton.setText("CLEAR SAVE DATA: " + clearSaveData);
            }
        });

        TextButton clearAllButton = new TextButton("CLEAR ALL DATA: " + clearSaveData, buttonStyle);
        clearAllButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                clearAllData = !clearAllData;
                clearAllButton.setText("CLEAR ALL DATA: " + clearAllData);
            }
        });

        TextButton applyButton = new TextButton("Apply & Exit", buttonStyle);
        applyButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                apply();
                game.setScreen(new MainMenuScreen(game));
            }
        });

        TextButton mainMenuButton = new TextButton("Exit Without Applying", buttonStyle);
        mainMenuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MainMenuScreen(game));
            }
        });

        table.add(settingsLabel).expandX().padBottom(20f);
        table.row();
        table.add(displayModeButton);
        table.row();
        table.add(inputWidth);
        table.row();
        table.add(inputHeight).padBottom(10f);
        table.row();
        table.add(fpsModeButton);
        table.row();
        table.add(vSyncModeButton);
        table.row();
        table.add(debugButton);
        table.row();
        table.add(clearButton);
        table.row();
        table.add(clearAllButton);
        table.row();
        table.add(applyButton).padTop(20f);
        table.row();
        table.add(mainMenuButton).padTop(20f);

        return table;
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float v) {
        Gdx.input.setInputProcessor(settingsStage);
        settingsStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        createPauseScreen();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
