package gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;

import inf112.minesweepers.Hud;
import inf112.minesweepers.LevelSwitcher;
import inf112.minesweepers.Pigsweepers;

import java.util.concurrent.TimeUnit;

public class WinScreen implements Screen {
	private final Pigsweepers game;
	private Stage stage;
	private final Texture winScreen;
	private BitmapFont gameOverFont;

	public WinScreen(Pigsweepers game) {
		this.game = game;
		winScreen = new Texture("gui/WinScreen.png");

		//Update time
		long time = game.save.getLong("time");
		game.save.putLong("time", time + LevelSwitcher.getStageTime());
		game.save.flush();

		createFont();
		createWinScreen();
	}

	private void createFont() {
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = 50;
		parameter.borderWidth = 2;
		gameOverFont = game.generator.generateFont(parameter);
	}

	private void createWinScreen() {
		float width = Gdx.graphics.getWidth();
		float height = Gdx.graphics.getHeight();

		stage = new Stage(new FitViewport(width, height), game.batch);
		stage.addActor(createTable(height));
	}

	private Table createTable(float height) {
		TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();
		buttonStyle.font = gameOverFont;

		long sec = game.save.getLong("time");
		long min = TimeUnit.SECONDS.toMinutes(sec);
		sec -= TimeUnit.MINUTES.toSeconds(min);

		Label gameOverLabel = new Label("You Win", new Label.LabelStyle(gameOverFont, Color.WHITE));
		Label timeLabel = new Label("Time: "+min+" min "+sec+" sec", new Label.LabelStyle(game.font, Color.WHITE));
		Label statsLabel = new Label("Diamonds: "+game.save.getInteger("diamonds")+"  |  Score: "+Hud.score, new Label.LabelStyle(game.font, Color.WHITE));

		TextButton newGameButton = new TextButton("New Game", buttonStyle);
		newGameButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				clearData();
				game.setScreen(new GameScreen(game));
			}
		});

		TextButton mainMenuButton = new TextButton("Main Menu", buttonStyle);
		mainMenuButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				clearData();
				game.setScreen(new MainMenuScreen(game));
			}
		});
		TextButton rageQuitButton = new TextButton("Quit", buttonStyle);
		rageQuitButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				clearData();
				Gdx.app.exit();
			}
		});

		Table table = new Table();
		// table.setDebug(game.debug); // turn on all debug lines
		table.center();
		table.setFillParent(true);

		table.add(gameOverLabel).expandX().padTop(height / 10);
		table.row().expandY();
		table.add(timeLabel).expandX().padTop(height / 10);
		table.row().expandY();
		table.add(statsLabel).expandX().padTop(height / 50);
		table.row().expandY();
		table.add(newGameButton).expandX().padTop(height / 20);
		table.row().expandY();
		table.add(mainMenuButton).expandX();
		table.row().expandY();
		table.add(rageQuitButton).expandX();
		table.row().expandY();

		return table;
	}

	private void clearData() {
		//Save total amount of diamonds to achievement file
		game.stats.putInteger("totalWins",game.stats.getInteger("totalWins")+1);

		int totalDiamonds = game.stats.getInteger("totalDiamonds");
		game.stats.putInteger("totalDiamonds",totalDiamonds + Hud.diamonds);

		//Save total time spent to file
		long time = game.save.getLong("time");
		game.stats.putLong("totalTime",game.stats.getLong("totalTime")+time);
		game.stats.flush();

		game.save.clear();
		game.save.flush();
		game.debug = false;
	}

	@Override
	public void show() {

	}

	@Override
	public void render(float delta) {
		game.batch.setProjectionMatrix(game.camera.combined);
		game.batch.begin();
		game.batch.draw(winScreen, 0, 0, game.camera.viewportWidth, game.camera.viewportHeight);
		game.batch.end();
		
		Gdx.input.setInputProcessor(stage);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		createWinScreen();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
